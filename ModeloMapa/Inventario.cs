﻿using System;

namespace Ur2.Modelo
{
    public class Inventario
    {
        private DateTime _dtInventario;
        private Local _local;

        private Pessoa _pessoa;

        public Inventario()
        {
            _dtInventario = DateTime.Now;
        }

        public Inventario(Local local, Pessoa pessoa)
        {
            _dtInventario = DateTime.Now;
            Local = local;
            Pessoa = pessoa;
        }

        public DateTime DtInventario
        {
            get { return _dtInventario; }
        }

        public string localRegistro
        {
            get
            {
                if (Local == null)
                    return string.Empty;
                return Local.Nome;
            }
        }

        public string registradoPor
        {
            get
            {
                if (Pessoa == null)
                    return string.Empty;
                return Pessoa.Nome;
            }
        }

        public PontoMapa ObtemPontoMapa()
        {
            return Local.PontoMapa;
        }

        internal Local Local
        {
            get { return _local; }
            set { _local = value; }
        }
        internal Pessoa Pessoa
        {
            get { return _pessoa; }
            set { _pessoa = value; }
        }
    }
}