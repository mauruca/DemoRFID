﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ur2.Modelo
{
    public class PadraoDepreciacao
    {
        private string _nome;

        public string Nome
        {
            get { return _nome; }
            set { _nome = value; }
        }

        private int _ciclomeses;

        public int Ciclomeses
        {
            get { return _ciclomeses; }
            set { _ciclomeses = value; }
        }

        private int _percentual;

        public int Percentual
        {
            get { return _percentual; }
            set { _percentual = value; }
        }

        public PadraoDepreciacao()
        { }

        public PadraoDepreciacao(string nome, int ciclomeses, int percentual)
        {
            Nome = nome;
            Ciclomeses = ciclomeses;
            Percentual = percentual;
        }
    }
}
