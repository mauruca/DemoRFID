﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Ur2.Modelo
{
    public class Depreciacao
    {
        private DateTime _dtDepreciacao;

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DtDepreciacao
        {
            get { return _dtDepreciacao; }
        }

        private decimal _valororiginal;
        private decimal _valordepreciado;

        public decimal ValorOriginal
        {
            get { return _valororiginal; }
            set { _valororiginal = value; }
        }

        public decimal ValorDepreciado
        {
            get { return _valordepreciado; }
            set { _valordepreciado = value; }
        }

        private int _percentualaplicado;

        public int PercentualAplicado
        {
            get { return _percentualaplicado; }
            set { _percentualaplicado = value; }
        }

        public Depreciacao()
        {
            _dtDepreciacao = DateTime.Today;
        }

        public Depreciacao(decimal valororiginal, decimal valordepreciado, int percentualaplicado)
        {
            _dtDepreciacao = DateTime.Today;
            ValorOriginal = valororiginal;
            ValorDepreciado = valordepreciado;
            PercentualAplicado = percentualaplicado;
        }
    }
}
