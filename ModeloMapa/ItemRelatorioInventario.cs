﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ur2.Modelo
{
    public class ItemRelatorioInventario
    {
        private string _tag;
        private string _descricaoBem;
        private string _localEncontrado;
        private string _localAnterior;
        private string _dataAquisicao;
        private string _dataUltimoInventario;
        private string _valorAquisicao;
        private string _valorAtual;
        private string _valorDepreciado;
        private string _situacaoBem;
        private string _estadoBem;

        public string Tag {
            get { return _tag; }
            set { _tag = value; }
        }
        public string DescricaoBem {
            get { return _descricaoBem; }
            set { _descricaoBem = value; }
        }
        public string LocalEncontrado {
            get { return _localEncontrado; }
            set { _localEncontrado = value; }
        }
        public string LocalAnterior
        {
            get { return _localAnterior; }
            set { _localAnterior = value; }
        }
        public string DataAquisicao
        {
            get { return _dataAquisicao; }
            set { _dataAquisicao = value; }
        }
        public string DataUltimoInventario {
            get { return _dataUltimoInventario; }
            set { _dataUltimoInventario = value; }
        }
        public string ValorAquisicao
        {
            get { return _valorAquisicao; }
            set { _valorAquisicao = value; }
        }
        public string ValorAtual
        {
            get { return _valorAtual; }
            set { _valorAtual = value; }
        }
        public string ValorDepreciado
        {
            get { return _valorDepreciado; }
            set { _valorDepreciado = value; }
        }
        public string SituacaoBem
        {
            get { return _situacaoBem; }
            set { _situacaoBem = value; }
        }
        public string EstadoBem
        {
            get { return _estadoBem; }
            set { _estadoBem = value; }
        }

        public ItemRelatorioInventario()
        {

        }

        public ItemRelatorioInventario(
            string tag, 
            string descricaoBem, 
            string localEncontrado, 
            string localAnterior, 
            string dataAquisicao, 
            string dataUltimoInventario,
            string valorAquisicao,
            string valorAtual,
            string valorDepreciado,
            string situacaoBem,
            string estadoBem
            )
        {
            Tag = tag;
            DescricaoBem = descricaoBem;
            LocalEncontrado = localEncontrado;
            LocalAnterior = localAnterior;
            DataAquisicao = dataAquisicao;
            DataUltimoInventario = dataUltimoInventario;
            ValorAquisicao = valorAquisicao;
            ValorAtual = valorAtual;
            ValorDepreciado = valorDepreciado;
            SituacaoBem = situacaoBem;
            EstadoBem = estadoBem;
        }

        private static string ApresentaData(DateTime data)
        {
            DateTime aux = new DateTime(0);
            if (data == aux)
                return "";
            return data.ToString("dd/MM/yyyy");
        }

        private static string ApresentaDinheiro(Decimal valor)
        {
            NumberFormatInfo nf = CultureInfo.CurrentCulture.NumberFormat.Clone() as NumberFormatInfo;
            nf.NumberDecimalSeparator = ",";
            nf.NumberGroupSeparator = ".";
            return String.Format(nf, "{0:0.00}", valor);
        }

        public static List<ItemRelatorioInventario> MontaRelatorio(List<Bem> bens)
        {
            List<ItemRelatorioInventario> retorno = new List<ItemRelatorioInventario>();
            List<Inventario> invs = new List<Inventario>();
            foreach (Bem b in bens)
            {
                ItemRelatorioInventario item = new ItemRelatorioInventario();
                item.Tag = b.Tag.Id;
                item.DescricaoBem = b.Descricao;
                item.LocalEncontrado = b.ObtemNomeLocalRegistroAtual();
                item._localAnterior = b.ObtemNomeLocalRegistroAnterior();
                item.DataAquisicao = ApresentaData(b.Data);
                item.DataUltimoInventario = ApresentaData(b.MaiorDataInventario());
                item.ValorAquisicao = ApresentaDinheiro(b.ValorAquisicao);
                item.ValorAtual = ApresentaDinheiro(b.ValorAtual);
                item.ValorDepreciado = ApresentaDinheiro(b.SomaValorDepreciado());
                item.SituacaoBem = b.Situacao;
                item.EstadoBem = b.Estado;
                retorno.Add(item);
            }
            return retorno;
        }


    }
}
