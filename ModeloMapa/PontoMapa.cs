﻿using System;
using System.Xml.Serialization;

namespace Ur2.Modelo
{
    public class PontoMapa
    {
        private double _latitude;

        private double _longitude;

        public double Latitude
        {
            get { return _latitude; }
            set
            {
                if (value > 90 || value < -90)
                    throw new ArgumentOutOfRangeException("Latitude", "Valor não pode ser maior que 90 e menor que -90.");
                _latitude = value;
            }
        }
        public double Longitude
        {
            get { return _longitude; }
            set
            {
                if (value > 180 || value < -180)
                    throw new ArgumentOutOfRangeException("Longitude", "Valor não pode ser maior que 180 e menor que -180.");
                _longitude = value;
            }
        }

        public PontoMapa()
        { }

        public PontoMapa(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        public override string ToString()
        {
            return Latitude.ToString() + " , " + Longitude.ToString();
        }
    }
}