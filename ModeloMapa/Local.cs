﻿using Ur2.RFID.Middleware.Modelo;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Ur2.Modelo
{
    public class Local
    {
        private string _nome;

        private PontoMapa _pontoMapa = new PontoMapa();

        private RFIDTag _tag = new RFIDTag();

        public string Nome
        {
            get { return _nome; }
            set { _nome = value.Trim(); }
        }
        public PontoMapa PontoMapa
        {
            get { return _pontoMapa; }
            set { _pontoMapa = value; }
        }
        public RFIDTag Tag
        {
            get { return _tag; }
            set { _tag = value; }
        }
        
        public Local()
        { 
        }

        public Local(string name, string tagid)
        {
            Nome = name;
            Tag.Id = tagid;
        }

        public Local(string name, string tagid, PontoMapa ponto)
        {
            Nome = name;
            Tag.Id = tagid;
            PontoMapa = ponto;
        }

        public bool souValido()
        {
            if (_tag == null)
                return false;
            if (_nome.Length == 0 || !_tag.isValid)
                return false;
            return true;
        }

        public bool souValidoMapa()
        {
            if (!souValido() || PontoMapa == null)
                return false;
            return true;
        }

        public override bool Equals(object obj)
        {
            return ((obj is Local) && (((Local)obj).Tag == this.Tag) && ((Local)obj).Nome.Equals(this.Nome) );
        }

        public static List<Local> ExistemLocais(List<Local> locais, List<RFIDTag> tags)
        {
            List<Local> listaLocaisIdentificados = new List<Local>();
            foreach (RFIDTag item in tags)
            {
                Local aux = locais.Find(loc => loc.Tag.Equals(item));
                if (aux == null)
                    continue;
                listaLocaisIdentificados.Add(aux);
            }
            return listaLocaisIdentificados;
        }

        public override int GetHashCode()
        {
            return Nome.GetHashCode() ^ PontoMapa.GetHashCode() ^ Tag.GetHashCode();
        }
    }
}