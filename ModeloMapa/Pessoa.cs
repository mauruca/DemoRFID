﻿using System.Collections.Generic;
using Ur2.RFID.Middleware.Modelo;
using System.Xml.Serialization;
using System.ComponentModel.DataAnnotations;

namespace Ur2.Modelo
{
    public class Pessoa
    {
        private string _nome;

        private RFIDTag _tag = new RFIDTag();

        private List<Bem> _usando = new List<Bem>();

        private List<Local> _locaisAutorizados = new List<Local>();

        public string Nome
        {
            get { return _nome; }
            set { _nome = value.Trim(); }
        }

        public RFIDTag Tag
        {
            get { return _tag; }
            set { _tag = value; }
        }

        public Pessoa()
        {
        }

        public Pessoa(string nome, string tagid)
        {
            Nome = nome;
            Tag.Id = tagid;
        }
        public List<Local> LocaisQuePossoIr()
        {
            return _locaisAutorizados;
        }

        public bool souValida()
        {
            if (_tag == null)
                return false;
            if (_nome.Length == 0 || !_tag.isValid)
                return false;
            return true;
        }


        public List<Bem> EquipamentosQueEstouUsando()
        {
            return _usando;
        }

        public void AdicionaEquipamentoEmUso(Bem bem)
        {
            if (bem.souValido())
            {
                if (!EstouUsandoEsteBem(bem))
                    _usando.Add(bem);
            }
        }

        public bool EstouUsandoEsteBem(Bem bem)
        {
            if ((_usando.Find(b => b.Equals(bem))) != null)
                return true;
            return false;
        }

        public void LimpaEquipamentosQueEstouUsando()
        {
            _usando.Clear();
        }
                
        public void AdicionaLocalAutorizado(Local local)
        {
            if (local.souValido())
                {
                    if (!PossoEntrarNesteLocal(local))
                        _locaisAutorizados.Add(local);                
                }
        }

        public void RemoveLocalAutorizado(Local local)
        {
            if (PossoEntrarNesteLocal(local))
            {
                _locaisAutorizados.Remove(local);
            }
        }

        public bool PossoEntrarNesteLocal(Local local)
        {
            if ((_locaisAutorizados.Find(loc => loc.Equals(local))) != null)
                return true;
            return false;
        }

        public static Pessoa EncontraPessoaIdTag(List<Pessoa> listaPessoas, string idTag)
        {
            return listaPessoas.Find(pessoa => pessoa.Tag.Id.Contains(idTag));
        }

        public static bool ExistePessoaIdTag(List<Pessoa> listaPessoas, string idTag)
        {
            Pessoa aux = EncontraPessoaIdTag(listaPessoas, idTag);

            if (aux != null)
            {
                return true;
            }
            return false;
        }

        public static List<Pessoa> EncontraPessoasIdTag(List<Pessoa> listaPessoas, string idTag)
        {
            return listaPessoas.FindAll(item => item.Tag.Id.Contains(idTag));
        }

        public static List<Pessoa> EncontraPessoasListaTag(List<Pessoa> listaPessoas, List<RFIDTag> tags)
        {
            List<Pessoa> listaPessoasIdentificadas = new List<Pessoa>();
            foreach (RFIDTag item in tags)
            {
                Pessoa aux = listaPessoas.Find(pes => pes.Tag.Equals(item));
                if (aux == null)
                    continue;
                listaPessoasIdentificadas.Add(aux);
            }
            return listaPessoasIdentificadas;
        }

        public override bool Equals(object obj)
        {
            return ((obj is Pessoa) && (((Pessoa)obj).Tag == this.Tag) && ((Pessoa)obj).Nome.Equals(this.Nome));
        }

        public override int GetHashCode()
        {
            return Nome.GetHashCode() ^ Tag.GetHashCode();
        }
    }
}