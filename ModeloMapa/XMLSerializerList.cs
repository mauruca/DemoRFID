﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Ur2.Modelo
{
    public class XMLSerializerList
    {
        public static List<T> Load<T>()
        {
            string filename = Environment.CurrentDirectory + "\\" + typeof(T).Name + ".xml";

            if (!File.Exists(filename))
                return new List<T>();

            // Now we can read the serialized book ...
            System.Xml.Serialization.XmlSerializer reader =
                new System.Xml.Serialization.XmlSerializer(typeof(List<T>));
            System.IO.StreamReader file = new System.IO.StreamReader(filename);

            List<T> retorno = (List<T>)reader.Deserialize(file);
            file.Close();
            return retorno;
        }

        public static void Save<T>(List<T> list)
        {
            string filename = Environment.CurrentDirectory + "\\" + typeof(T).Name + ".xml";

            System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(List<T>));
            System.IO.FileStream file = System.IO.File.Create(filename);

            writer.Serialize(file, list);
            file.Close();
        }
    }
}
