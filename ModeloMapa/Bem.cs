﻿using System;
using System.Collections.Generic;
using Ur2.RFID.Middleware.Modelo;
using System.Linq;

namespace Ur2.Modelo
{
    public class Bem
    {
        private DateTime _data;
        private string _descricao;

        private List<Inventario> _inventarios = new List<Inventario>();
        private List<Depreciacao> _depreciacoes = new List<Depreciacao>();

        private RFIDTag _tag = new RFIDTag();

        private decimal _valoraquisicao;
        private decimal _valoratual;

        public DateTime Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public string Descricao
        {
            get { return _descricao; }
            set { _descricao = value; }
        }

        public RFIDTag Tag
        {
            get { return _tag; }
            set { _tag = value; }
        }
        
        public decimal ValorAtual
        {
            get { return _valoratual; }
            set { _valoratual = value; }
        }
        public decimal ValorAquisicao
        {
            get { return _valoraquisicao; }
            set { _valoraquisicao = value; }
        }

        private PadraoDepreciacao _padraoDepreciacao;

        public string PadraoDepreciacao
        {
            get {
                if (_padraoDepreciacao == null)
                    return string.Empty;
                return _padraoDepreciacao.Nome;
            }
            /*
            set {
                List<PadraoDepreciacao> padroes = XMLSerializerList.Load<PadraoDepreciacao>();
                foreach (PadraoDepreciacao item in padroes)
                {
                    if (item.Nome == value)
                    {
                        _padraoDepreciacao = item;
                        return;
                    }
                }
            }*/
        }

        private string _situacao;

        public string Situacao
        {
            get { return _situacao; }
            set { _situacao = value; }
        }

        private string _estado;

        public string Estado
        {
            get { return _estado; }
            set { _estado = value; }
        }

        public Bem()
        {
        }

        public Bem(string descricao, DateTime data, decimal valor, string tag)
        {
            Descricao = descricao;
            Data = data;
            _valoraquisicao = valor;
            _valoratual = _valoraquisicao;
            Tag.Id = tag;
        }

        public Bem(string descricao, DateTime data, decimal valor, string tag, string situacao, string estado)
        {
            Descricao = descricao;
            Data = data;
            _valoraquisicao = valor;
            _valoratual = _valoraquisicao;
            Tag.Id = tag;
            Situacao = situacao;
            Estado = estado;
        }

        public void AdicionaPadraoDepreciacao(PadraoDepreciacao padrao = null)
        {
            if (padrao == null)
                return;
            _padraoDepreciacao = padrao;
        }

        public PadraoDepreciacao ObtemPadraoDepreciacao()
        {
            return _padraoDepreciacao;
        }

        public bool souValido()
        {
            if (_tag == null)
                return false;
            if (_descricao.Length == 0 || !_tag.isValid)
                return false;
            return true;
        }

        public List<Inventario> ObtemInventarios()
        {
            return _inventarios;
        }

        public List<Depreciacao> ObtemDepreciacoes()
        {
            return _depreciacoes;
        }

        public List<Inventario> ObtemInventariosOrdenado()
        {
            return _inventarios.OrderByDescending(i => i.DtInventario).ToList();
        }

        public List<Depreciacao> ObtemDepreciacoesOrdenado()
        {
            return _depreciacoes.OrderByDescending(i => i.DtDepreciacao).ToList();
        }

        public DateTime MaiorDataDepreciacao()
        {
            DateTime retorno = new DateTime(0);
            List<Depreciacao> lista = ObtemDepreciacoesOrdenado();
            if (lista == null || lista.Count == 0)
                return retorno;
            return lista[0].DtDepreciacao;
        }

        public decimal SomaValorDepreciado()
        {
            decimal valor = 0;
            List<Depreciacao> lista = this.ObtemDepreciacoesOrdenado();
            if (lista == null || lista.Count == 0)
                return 0;
            foreach (Depreciacao item in lista)
            {
                valor += item.ValorDepreciado;
            }
            return valor;
        }

        public DateTime MaiorDataInventario()
        {
            DateTime retorno = new DateTime(0);
            List<Inventario> lista = this.ObtemInventariosOrdenado();
            if (lista == null || lista.Count == 0)
               return retorno;
            return lista[0].DtInventario;
        }

        public string ObtemNomeLocalRegistroAtual()
        {
            List<Inventario> lista = this.ObtemInventariosOrdenado();
            if (lista == null || lista.Count == 0)
                return null;
            return lista[0].localRegistro;
        }

        public string ObtemNomeLocalRegistroAnterior()
        {
            List<Inventario> lista = this.ObtemInventariosOrdenado();
            if (lista == null || lista.Count == 0)
                return null;
            if(lista.Count == 1)
                return lista[0].localRegistro;
            return lista[1].localRegistro;
        }

        #region Statics
        public static Bem EncontraBemIdTag(List<Bem> listaBem, string idTag)
        {
            return listaBem.Find(pessoa => pessoa.Tag.Id.Contains(idTag));
        }

        public static bool ExisteBemIdTag(List<Bem> listaBem, string idTag)
        {
            Bem aux = EncontraBemIdTag(listaBem, idTag);

            if (aux != null)
            {
                return true;
            }
            return false;
        }

        public static List<Bem> EncontraBensIdTag(List<Bem> listaBem, string idTag)
        {
            return listaBem.FindAll(item => item.Tag.Id.Contains(idTag));
        }

        public static List<Bem> EncontraBemListaTag(List<Bem> listaBem, List<RFIDTag> tags)
        {
            List<Bem> listaBemIdentificadas = new List<Bem>();
            foreach (RFIDTag item in tags)
            {
                Bem aux = listaBem.Find(pes => pes.Tag.Id.Contains(item.Id));
                if (aux == null)
                    continue;
                listaBemIdentificadas.Add(aux);
            }
            return listaBemIdentificadas;
        }
        #endregion Statics

        public override bool Equals(object obj)
        {
            return ((obj is Bem) && (((Bem)obj)._tag == this._tag) && ((Bem)obj).Descricao.Equals(this.Descricao) );
        }

        public override int GetHashCode()
        {
            return Data.GetHashCode() ^ Descricao.GetHashCode() ^ Tag.GetHashCode() ^ ValorAquisicao.GetHashCode() ^ ValorAtual.GetHashCode(); 
        }
    }
}