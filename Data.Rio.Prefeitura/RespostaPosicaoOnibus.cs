﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Ur2.Data.Rio.Prefeitura
{
    [DataContract]
    public class RespostaPosicaoOnibus
    {
        [DataMember(Name = "COLUMNS")]
        public string[] colunas;
        [DataMember(Name = "DATA")]
        public object[] dados;
    }
}
