﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ur2.Modelo;
using System.IO;
using System.Runtime.Serialization;

namespace Ur2.Data.Rio.Prefeitura
{
    [DataContract]
    public class PosicaoOnibus
    {
        private string _datahora;

        private string _linha;

        private string _ordem;

        private decimal _latitude;

        private decimal _longitude;

        private decimal _velocidade;

        private string _direcao;


        [DataMember(Name="DATAHORA")]
        public string DATAHORA
        {
            get { return _datahora; }
            set { _datahora = value; }
        }
        [DataMember(Name = "ORDEM")]
        public string ORDEM
        {
            get { return _ordem; }
            set { _ordem = value; }
        }
        [DataMember(Name="LINHA")]
        public string LINHA
        {
            get { return _linha; }
            set { _linha = value; }
        }
        [DataMember(Name="LATITUDE")]
        public decimal LATITUDE
        {
            get { return _latitude; }
            set { _latitude = value; }
        }
        [DataMember(Name="LONGITUDE")]
        public decimal LONGITUDE
        {
            get { return _longitude; }
            set { _longitude = value; }
        }
        [DataMember(Name="VELOCIDADE")]
        public decimal VELOCIDADE
        {
            get { return _velocidade; }
            set { _velocidade = value; }
        }
        [DataMember(Name="DIRECAO")]
        public string DIRECAO
        {
          get { return _direcao; }
          set { _direcao = value; }
        }

        public bool souValida()
        {
            if (ORDEM != null && ORDEM.Length > 0)
                return true;
            return false;
        }

        public PosicaoOnibus(string datahora, string ordem, string linha, decimal latitude, decimal longitude, decimal velocidade, string direcao)
        {
            DATAHORA = datahora;
            ORDEM = ordem;
            LINHA = linha;
            LATITUDE = latitude;
            LONGITUDE = longitude;
            VELOCIDADE = velocidade;
            DIRECAO = direcao;
        }
    }
}
