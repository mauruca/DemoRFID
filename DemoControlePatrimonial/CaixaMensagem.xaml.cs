﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Ur2.RFID.Demo
{
    /// <summary>
    /// Interaction logic for CaixaMensagem.xaml
    /// </summary>
    public partial class CaixaMensagem : Window
    {
        public CaixaMensagem()
        {
            InitializeComponent();
            this.Width = 300;
            this.Height = 150;
        }

        public CaixaMensagem(int width, int height)
        {
            this.Width = width;
            this.Height = height;
        }

        public void DefineMensagem(string mensagem)
        {
            MensagemTxb.Text = mensagem;
        }

        private void MessageBoxCanvasCloseBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }
    }
}
