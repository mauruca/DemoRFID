﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Ur2.RFID.Middleware;
using Ur2.RFID.Middleware.Modelo;
using Ur2.Modelo;

namespace Ur2.RFID.Demo
{
    /// <summary>
    /// Interaction logic for MonitorPlanta.xaml
    /// </summary>
    public partial class MonitorPlanta : Window
    {
        List<Local> locais = new List<Local>();
        private IDevice dispositivoAtual;

        public MonitorPlanta()
        {
            InitializeComponent();
        }

        public void DefineLocais(List<Local> lista)
        {
            locais = lista;
        }

        private void IniciaLeitura(bool continuo = false)
        {
            if (dispositivoAtual == null)
                return;

            dispositivoAtual.ConfigTickerChangeElapseTime();
            DeviceAtivo();
            dispositivoAtual.Memory.Clear();
            dispositivoAtual.StartReading(continuo);
        }

        private void ParaLeitura()
        {
            if (dispositivoAtual != null)
                dispositivoAtual.StopReading();
            DeviceInativo();
        }

        private void DeviceAtivo()
        {
            AntenaAtivoBt.Visibility = Visibility.Visible;
        }

        private void DeviceInativo()
        {
            AntenaAtivoBt.Visibility = Visibility.Hidden;
        }

        private void IniciaLeituraInventario()
        {
            dispositivoAtual = DeviceFactory.CreateDevice(ConstantesDemoLocais.dispositivo);

            if (dispositivoAtual == null)
                return;

            ParaLeituraInventario();
            IniciaLeituraPeriodica();
            IniciaLeitura(true);
        }
        private void ParaLeituraInventario()
        {
            ParaLeitura();
            ParaLeituraPeriodica();
        }

        private void IniciaLeituraPeriodica()
        {
            if (dispositivoAtual == null)
                return;

            dispositivoAtual.OnTimerTick += new ChangeEventHandler(AcionaLocalPlanta);
            dispositivoAtual.StartTickerChange();
        }

        private void ParaLeituraPeriodica()
        {
            if (dispositivoAtual == null)
                return;
            dispositivoAtual.StopTickerChange();
            dispositivoAtual.OnTimerTick -= new ChangeEventHandler(AcionaLocalPlanta);
        }

        private void AcionaLocalPlanta()
        {
            DesativaTodasAreas();

            if (dispositivoAtual == null)
                return;

            List<RFIDTag> memoryTags = dispositivoAtual.Memory.Tags;
            foreach (RFIDTag item in memoryTags)
            {
                List<Local> locais = EncontraLocais(item.Id);
                if (locais == null || locais.Count == 0)
                    continue;
                foreach (Local loc in locais)
                {
                    AtivaItemPlanta(loc);
                }
            }
            dispositivoAtual.Memory.Clear();
        }

        private List<Local> EncontraLocais(string tagid)
        {
            return locais.FindAll(item => item.Tag.Id.Contains(tagid));
        }

        private void AtivaItemPlanta(Local local)
        {
            switch (local.Nome)
            {
                case ConstantesDemoLocais.EstoqueMateriaPrima:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => AtivaEstoqueMateriaPrima());
                        break;
                    }
                    AtivaEstoqueMateriaPrima();
                    break;
                case ConstantesDemoLocais.Laboratorio:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => AtivaLaboratorio());
                        break;
                    }
                    AtivaLaboratorio();
                    break;
                case ConstantesDemoLocais.ExpedicaoFabrica:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => AtivaExpedicao());
                        break;
                    }
                    AtivaExpedicao();
                    break;
                case ConstantesDemoLocais.Envase:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => AtivaEnvase());
                        break;
                    }
                    AtivaEnvase();
                    break;
                case ConstantesDemoLocais.Fabrica:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => AtivaFabrica());
                        break;
                    }
                    AtivaFabrica();
                    break;
                default:
                    break;
            }
        }

        private void DesativaTodasAreas()
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => DesativaEstoqueMateriaPrima());
                Dispatcher.Invoke(() => DesativaLaboratorio());
                Dispatcher.Invoke(() => DesativaExpedicao());
                Dispatcher.Invoke(() => DesativaEnvase());
                Dispatcher.Invoke(() => DesativaFabrica());
                return;
            }
            DesativaEstoqueMateriaPrima();
            DesativaLaboratorio();
            DesativaExpedicao();
            DesativaEnvase();
            DesativaFabrica();
        }

        delegate void AtivaItem();

        #region Areas
        private void AtivaEstoqueMateriaPrima()
        {
            EstoqueMateriaPrima.Visibility = Visibility.Visible;
        }
        private void DesativaEstoqueMateriaPrima()
        {
            EstoqueMateriaPrima.Visibility = Visibility.Hidden;
        }
        private void AtivaLaboratorio()
        {
            Laboratorio.Visibility = Visibility.Visible;
        }
        private void DesativaLaboratorio()
        {
            Laboratorio.Visibility = Visibility.Hidden;
        }
        private void AtivaExpedicao()
        {
            Expedicao.Visibility = Visibility.Visible;
        }
        private void DesativaExpedicao()
        {
            Expedicao.Visibility = Visibility.Hidden;
        }
        private void AtivaEnvase()
        {
            Envase.Visibility = Visibility.Visible;
        }
        private void DesativaEnvase()
        {
            Envase.Visibility = Visibility.Hidden;
        }
        private void AtivaFabrica()
        {
            Fabrica.Visibility = Visibility.Visible;
        }
        private void DesativaFabrica()
        {
            Fabrica.Visibility = Visibility.Hidden;
        }
        #endregion Areas

        private void MoveTela(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void FecharBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ParaLeituraInventario();
            if (dispositivoAtual != null)
               dispositivoAtual.Memory.Clear();
            DesativaTodasAreas();
            this.Hide();
        }

        private void AntenaBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            IniciaLeituraInventario();
        }

        private void AntenaAtivoBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ParaLeituraInventario();
        }
        
        private void MoveBt_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MoveTela(sender, e);
        }
    }
}
