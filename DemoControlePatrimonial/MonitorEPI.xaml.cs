﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Ur2.Modelo;
using Ur2.RFID.Middleware;
using Ur2.RFID.Middleware.Modelo;

namespace Ur2.RFID.Demo
{
    /// <summary>
    /// Interaction logic for MonitorEPI.xaml
    /// </summary>
    public partial class MonitorEPI : Window
    {
        List<Pessoa> pessoas = new List<Pessoa>();
        List<Bem> bens = new List<Bem>();
        List<Local> locais = new List<Local>();
        List<RFIDTag> tagsMemoria = new List<RFIDTag>();
        List<Pessoa> funcionariosAptos = new List<Pessoa>();

        private IDevice dispositivoAtual;
        
        public MonitorEPI()
        {
            InitializeComponent();
            ReiniciaOpacidadeEPIRegistroEntrada();
        }

        public void DefinePessoas(List<Pessoa> lista)
        {
            pessoas = lista;
        }

        public void DefineBens(List<Bem> lista)
        {
            bens = lista;
        }

        public void DefineLocais(List<Local> lista)
        {
            locais = lista;
        }

        private void MudaOpacidadeImage(Image objeto, bool metade = false)
        {
            if (metade)
            {
                objeto.Opacity = 0.5;
                return;
            }
            objeto.Opacity = 1;
        }

        private void MudaOpacidadeImageThread(Image objeto, bool metade = false)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => MudaOpacidadeImage(objeto, metade));
                return;
            }
            MudaOpacidadeImage(objeto, metade);
        }

        private void MudaOpacidadeElipse(Ellipse objeto, bool metade = false)
        {
            if (metade)
            {
                objeto.Opacity = 0.2;
                return;
            }
            objeto.Opacity = 1;
        }

        private void MudaOpacidadeElipseThread(Ellipse objeto, bool metade = false)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => MudaOpacidadeElipse(objeto, metade));
                return;
            }
            MudaOpacidadeElipse(objeto, metade);
        }

        private void AlteraFuncionarioAutorizacaoLocalThread(string nomeFuncionario)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => AlteraFuncionarioAutorizacaoLocal(nomeFuncionario));
                return;
            }
            AlteraFuncionarioAutorizacaoLocal(nomeFuncionario);
        }

        private void AlteraFuncionarioAutorizacaoLocal(string nomeFuncionario)
        {
            FuncionarioEncontradoLbl.Content = nomeFuncionario;
        }

        private void IniciaLeitura(bool continuo = false)
        {
            if (dispositivoAtual == null)
                return;

            dispositivoAtual.Memory.Clear();
            dispositivoAtual.StartReading(continuo);
        }

        private void ParaLeitura()
        {
            if(dispositivoAtual != null)
                dispositivoAtual.StopReading();
        }

        #region RegistroEntrada
        private void MudaSemaforoRegistroEntrada(bool verde = false)
        {
            if (verde)
            {
                MudaOpacidadeElipseThread(SinalVerde);
                MudaOpacidadeElipseThread(SinalVermelho, true);
            }
            else
            {
                MudaOpacidadeElipseThread(SinalVerde, true);
                MudaOpacidadeElipseThread(SinalVermelho);
            }
        }

        private void ReiniciaOpacidadeEPIRegistroEntrada()
        {
            MudaOpacidadeImageThread(PessoaEPI, true);
            MudaOpacidadeImageThread(CapaceteEPI, true);
            MudaOpacidadeImageThread(LuvasEPI, true);
            MudaOpacidadeImageThread(CintoEPI, true);
            MudaSemaforoRegistroEntrada();
        }
        private void DeviceRegistroEntradaAtivo()
        {
            AntenaRegistroAtivoBt.Visibility = Visibility.Visible;
        }

        private void DeviceRegistroEntradaInativo()
        {
            AntenaRegistroAtivoBt.Visibility = Visibility.Hidden;
        }
        private void IniciaLeituraPeriodicaRegistroEntrada()
        {
            if (dispositivoAtual == null)
                return;

            dispositivoAtual.ConfigTickerChangeElapseTime(5);
            dispositivoAtual.OnTimerTick += new ChangeEventHandler(AvaliaLeituraRegistroEntrada);
            dispositivoAtual.StartTickerChange();
        }

        private void ParaLeituraPeriodicaRegistroEntrada()
        {
            if (dispositivoAtual == null)
                return;

            dispositivoAtual.StopTickerChange();
            dispositivoAtual.OnTimerTick -= new ChangeEventHandler(AvaliaLeituraRegistroEntrada);
            dispositivoAtual.ConfigTickerChangeElapseTime(1);
        }

        private void IniciaLeituraInventarioRegistroEntrada()
        {
            dispositivoAtual = DeviceFactory.CreateDevice(ConstantesDemoLocais.dispositivo);

            if (dispositivoAtual == null)
                return;

            ParaLeituraInventarioRegistroEntrada();
            ReiniciaOpacidadeEPIRegistroEntrada();
            IniciaLeituraPeriodicaRegistroEntrada();
            DeviceRegistroEntradaAtivo();
            IniciaLeitura(true);
        }

        private void ParaLeituraInventarioRegistroEntrada()
        {
            ParaLeitura();
            DeviceRegistroEntradaInativo();
            ParaLeituraPeriodicaRegistroEntrada();
            ReiniciaOpacidadeEPIRegistroEntrada();
        }

        private void AvaliaLeituraRegistroEntrada()
        {
            tagsMemoria.Clear();
            if (!dispositivoAtual.Memory.isLastTagValid)
            {
                dispositivoAtual.Memory.Clear();
                return;
            }

            if (dispositivoAtual == null)
                return;

            foreach (RFIDTag item in dispositivoAtual.Memory.Tags)
            {
                tagsMemoria.Add(item);
            }
            dispositivoAtual.Memory.Clear();

            VerificaPessoaEPI();
        }

        private void VerificaPessoaEPI()
        {
            bool sinalverde = true;
            Bem capacete = null;
            Bem luva = null;
            Bem cinto = null;

            // encontro a pessoa
            List<Pessoa> pessoasLidas = Pessoa.EncontraPessoasListaTag(pessoas, tagsMemoria);
            if (pessoasLidas.Count == 0 || Pessoa.ExistePessoaIdTag(funcionariosAptos, pessoasLidas[0].Tag.Id))
            {
                ReiniciaOpacidadeEPIRegistroEntrada();
                return;
            }
            else
                MudaOpacidadeImageThread(PessoaEPI);


            List<Bem> bensLidos = Bem.EncontraBemListaTag(bens, tagsMemoria);
            if (bensLidos.Count == 0)
            {
                MudaOpacidadeImageThread(CapaceteEPI, true);
                MudaOpacidadeImageThread(LuvasEPI, true);
                MudaOpacidadeImageThread(CintoEPI, true);
                sinalverde = false;
            }
            else
            {
                // encontro capacete
                capacete = bensLidos.Find(item => item.Descricao.ToUpper().Contains("CAPACETE") == true);
                if (capacete == null)
                {
                    MudaOpacidadeImageThread(CapaceteEPI, true);
                    sinalverde = false;
                }
                else
                    MudaOpacidadeImageThread(CapaceteEPI);

                // encontro luvas
                luva = bensLidos.Find(item => item.Descricao.ToUpper().Contains("LUVA") == true);
                if (luva == null)
                {
                    MudaOpacidadeImageThread(LuvasEPI, true);
                    sinalverde = false;
                }
                else
                    MudaOpacidadeImageThread(LuvasEPI);

                // encontro cinto
                cinto = bensLidos.Find(item => item.Descricao.ToUpper().Contains("CINTO") == true);
                if (cinto == null)
                {
                    MudaOpacidadeImageThread(CintoEPI, true);
                    sinalverde = false;
                }
                else
                    MudaOpacidadeImageThread(CintoEPI);

                MudaSemaforoRegistroEntrada(sinalverde);

                if (sinalverde)
                {
                    pessoasLidas[0].AdicionaEquipamentoEmUso(capacete);
                    pessoasLidas[0].AdicionaEquipamentoEmUso(luva);
                    pessoasLidas[0].AdicionaEquipamentoEmUso(cinto);
                    RegistraFuncionarioApto(pessoasLidas[0]);
                }
            }
        }

        private void RegistraFuncionarioApto(Pessoa funcionario)
        {
            if (Pessoa.ExistePessoaIdTag(funcionariosAptos, funcionario.Tag.Id))
                return;

            funcionariosAptos.Add(funcionario);
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => FuncionariosAptosDg.ItemsSource = funcionariosAptos);
                Dispatcher.Invoke(() => FuncionariosAptosDg.Items.Refresh());
                return;
            }
            FuncionariosAptosDg.Items.Refresh();
        }
        #endregion RegistroEntrada

        #region AutorizacaoLocal
        private void ReiniciaOpacidadeEPIAutorizacaoLocal()
        {
            AlteraFuncionarioAutorizacaoLocalThread("");
            MudaSemaforoAutorizacaoLocal();
        }
        private void MudaSemaforoAutorizacaoLocal(bool verde = false)
        {
            if (verde)
            {
                MudaOpacidadeElipseThread(SinalVerdeAutorizacao);
                MudaOpacidadeElipseThread(SinalVermelhoAutorizacao, true);
            }
            else
            {
                MudaOpacidadeElipseThread(SinalVerdeAutorizacao, true);
                MudaOpacidadeElipseThread(SinalVermelhoAutorizacao);
            }
        }

        private void MarcaLocalObra(Local local)
        {
            DesmarcaTodosLocaisObra();
            switch (local.Nome)
            {
                case ConstantesDemoLocais.Setor1:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => AtivaDesativaSetorObra(Setor1, true));
                        break;
                    }
                    AtivaDesativaSetorObra(Setor1, true);
                    break;
                case ConstantesDemoLocais.Setor2:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => AtivaDesativaSetorObra(Setor2, true));
                        break;
                    }
                    AtivaDesativaSetorObra(Setor2, true);
                    break;
                case ConstantesDemoLocais.Setor3:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => AtivaDesativaSetorObra(Setor3, true));
                        break;
                    }
                    AtivaDesativaSetorObra(Setor3, true);
                    break;
                case ConstantesDemoLocais.Setor4:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => AtivaDesativaSetorObra(Setor4, true));
                        break;
                    }
                    AtivaDesativaSetorObra(Setor4, true);
                    break;
                case ConstantesDemoLocais.Setor5:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => AtivaDesativaSetorObra(Setor5, true));
                        break;
                    }
                    AtivaDesativaSetorObra(Setor5, true);
                    break;            
            }
        }

        private void AtivaDesativaSetorObra(UniformGrid setor, bool ativa = false)
        {
            if(ativa)
            {
                setor.Visibility = Visibility.Visible;
                return;
            }
            setor.Visibility = Visibility.Hidden;

        }

        private void DesmarcaTodosLocaisObra()
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => AtivaDesativaSetorObra(Setor1));
                Dispatcher.Invoke(() => AtivaDesativaSetorObra(Setor2));
                Dispatcher.Invoke(() => AtivaDesativaSetorObra(Setor3));
                Dispatcher.Invoke(() => AtivaDesativaSetorObra(Setor4));
                Dispatcher.Invoke(() => AtivaDesativaSetorObra(Setor5));
                return;
            }
            AtivaDesativaSetorObra(Setor1);
            AtivaDesativaSetorObra(Setor2);
            AtivaDesativaSetorObra(Setor3);
            AtivaDesativaSetorObra(Setor4);
            AtivaDesativaSetorObra(Setor5);
        }

        private void DeviceAutorizacaoLocalAtivo()
        {
            AntenaAutorizacaoAtivoBt.Visibility = Visibility.Visible;
        }

        private void DeviceAutorizacaoLocalInativo()
        {
            AntenaAutorizacaoAtivoBt.Visibility = Visibility.Hidden;
        }

        private void IniciaLeituraPeriodicaAutorizacaoLocal()
        {
            if (dispositivoAtual == null)
                return;

            dispositivoAtual.ConfigTickerChangeElapseTime(5);
            dispositivoAtual.OnTimerTick += new ChangeEventHandler(AvaliaLeituraAutorizacaoLocal);
            dispositivoAtual.StartTickerChange();
        }

        private void ParaLeituraPeriodicaAutorizacaoLocal()
        {
            if (dispositivoAtual == null)
                return;

            dispositivoAtual.StopTickerChange();
            dispositivoAtual.OnTimerTick -= new ChangeEventHandler(AvaliaLeituraAutorizacaoLocal);
            dispositivoAtual.ConfigTickerChangeElapseTime(1);
        }

        private void IniciaLeituraInventarioAutorizacaoLocal()
        {
            dispositivoAtual = DeviceFactory.CreateDevice(ConstantesDemoLocais.dispositivo);

            if (dispositivoAtual == null)
                return;

            ParaLeituraInventarioAutorizacaoLocal();
            ReiniciaOpacidadeEPIAutorizacaoLocal();
            IniciaLeituraPeriodicaAutorizacaoLocal();
            DeviceAutorizacaoLocalAtivo();
            IniciaLeitura(true);
        }

        private void ParaLeituraInventarioAutorizacaoLocal()
        {
            ParaLeitura();
            DeviceAutorizacaoLocalInativo();
            ParaLeituraPeriodicaAutorizacaoLocal();
            ReiniciaOpacidadeEPIAutorizacaoLocal();
        }

        private void AvaliaLeituraAutorizacaoLocal()
        {
            tagsMemoria.Clear();

            if (dispositivoAtual == null)
                return;

            if (!dispositivoAtual.Memory.isLastTagValid)
            {
                dispositivoAtual.Memory.Clear();
                return;
            }

            foreach (RFIDTag item in dispositivoAtual.Memory.Tags)
            {
                tagsMemoria.Add(item);
            }
            dispositivoAtual.Memory.Clear();

            ValidaPessoaObra();
        }
        
        private void ValidaPessoaObra()
        {
            Pessoa pessoaSelecionada = null;
            Local localSelecionado = null;

            ReiniciaOpacidadeEPIAutorizacaoLocal();
            MudaSemaforoAutorizacaoLocal();

            // encontro a pessoa
            List<Pessoa> pessoasLidas = Pessoa.EncontraPessoasListaTag(pessoas, tagsMemoria);
            if (pessoasLidas.Count == 0)
            {
                return;
            }

            pessoaSelecionada = pessoasLidas[0];

            if (!Pessoa.ExistePessoaIdTag(funcionariosAptos, pessoaSelecionada.Tag.Id))
            {
                return;
            }

            AlteraFuncionarioAutorizacaoLocalThread(pessoaSelecionada.Nome);

            List<Local> locaisLidos = Local.ExistemLocais(locais, tagsMemoria);
            if (locaisLidos.Count == 0)
            {
                return;
            }

            locaisLidos = ConstantesDemoLocais.LocaisObra(locaisLidos);
            if (locaisLidos.Count == 0)
            {
                return;
            }

            localSelecionado = locaisLidos[0];
            MarcaLocalObra(localSelecionado);

            if (pessoaSelecionada.PossoEntrarNesteLocal(localSelecionado))
            {
                MudaSemaforoAutorizacaoLocal(true);
                return;
            }
            RegistraLocalAutorizado(pessoaSelecionada, localSelecionado);
        }

        private void RegistraLocalAutorizado(Pessoa funcionario, Local local)
        {
            if (funcionario == null || local == null)
                return;
            if (funcionario.PossoEntrarNesteLocal(local))
                return;
            if (funcionario.LocaisQuePossoIr().Count > 0)
                return;

            funcionario.AdicionaLocalAutorizado(local);
        }

        #endregion AutorizacaoLocal

        #region MonitoriaAcesso
        private void DeviceAutorizacaoMonitoriaAcesso()
        {
            AntenaMonitoriaAcessoAtivoBt.Visibility = Visibility.Visible;
        }

        private void DeviceMonitoriaAcessoInativo()
        {
            AntenaMonitoriaAcessoAtivoBt.Visibility = Visibility.Hidden;
        }
        private void IniciaLeituraPeriodicaMonitoriaAcesso()
        {
            if (dispositivoAtual == null)
                return;

            dispositivoAtual = DeviceFactory.CreateDevice(ConstantesDemoLocais.dispositivo);
            dispositivoAtual.ConfigTickerChangeElapseTime(5);
            dispositivoAtual.OnTimerTick += new ChangeEventHandler(AvaliaLeituraMonitoriaAcesso);
            dispositivoAtual.StartTickerChange();
        }

        private void ParaLeituraPeriodicaMonitoriaAcesso()
        {
            if (dispositivoAtual == null)
                return;

            dispositivoAtual.StopTickerChange();
            dispositivoAtual.OnTimerTick -= new ChangeEventHandler(AvaliaLeituraMonitoriaAcesso);
            dispositivoAtual.ConfigTickerChangeElapseTime(1);
        }

        private void IniciaLeituraInventarioMonitoriaAcesso()
        {
            dispositivoAtual = DeviceFactory.CreateDevice(ConstantesDemoLocais.dispositivo);

            if (dispositivoAtual == null)
                return;

            ParaLeituraInventarioMonitoriaAcesso();
            IniciaLeituraPeriodicaMonitoriaAcesso();
            DeviceAutorizacaoMonitoriaAcesso();
            IniciaLeitura(true);
        }

        private void ParaLeituraInventarioMonitoriaAcesso()
        {
            ParaLeitura();
            DeviceMonitoriaAcessoInativo();
            ParaLeituraPeriodicaMonitoriaAcesso();
            ReiniciaOpacidadeEPIAutorizacaoLocal();
        }

        private void AvaliaLeituraMonitoriaAcesso()
        {
            tagsMemoria.Clear();

            if (dispositivoAtual == null)
                return;

            if (!dispositivoAtual.Memory.isLastTagValid)
            {
                dispositivoAtual.Memory.Clear();
                return;
            }

            foreach (RFIDTag item in dispositivoAtual.Memory.Tags)
            {
                tagsMemoria.Add(item);
            }
            dispositivoAtual.Memory.Clear();

            ValidaAcessoObra();
        }

        private void ValidaAcessoObra()
        {
            Pessoa pessoaSelecionada = null;
            Local localSelecionado = null;

            DesmarcaTodosLocaisObra();

            // encontro a pessoa
            List<Pessoa> pessoasLidas = Pessoa.EncontraPessoasListaTag(pessoas, tagsMemoria);
            if (pessoasLidas.Count == 0)
            {
                return;
            }

            pessoaSelecionada = pessoasLidas[0];

            if (!Pessoa.ExistePessoaIdTag(funcionariosAptos, pessoaSelecionada.Tag.Id))
            {
                return;
            }

            List<Local> locaisLidos = Local.ExistemLocais(locais, tagsMemoria);
            if (locaisLidos.Count == 0)
            {
                return;
            }

            locaisLidos = ConstantesDemoLocais.LocaisObra(locaisLidos);
            if (locaisLidos.Count == 0)
            {
                return;
            }

            localSelecionado = locaisLidos[0];

            if (!pessoaSelecionada.PossoEntrarNesteLocal(localSelecionado))
            {
                MarcaLocalObra(localSelecionado);
                return;
            }
        }

        #endregion MonitoriaAcesso

        private void MoveTela(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void MoveBt_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MoveTela(sender, e);
        }

        private void RegistroMoveBtFecharBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            this.Hide();
        }

        private void FecharBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            this.Hide();
        }

        private void AntenaRegistroBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            IniciaLeituraInventarioRegistroEntrada();
        }

        private void AntenaRegistroAtivoBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ParaLeituraInventarioRegistroEntrada();
        }

        private void AntenaAutorizacaoBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            IniciaLeituraInventarioAutorizacaoLocal();
        }

        private void AntenaAutorizacaoAtivoBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ParaLeituraInventarioAutorizacaoLocal();
        }

        private void AntenaMonitoriaAcessoBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            IniciaLeituraInventarioMonitoriaAcesso();
        }

        private void AntenaMonitoriaAcessoAtivoBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ParaLeituraInventarioMonitoriaAcesso();
        }
    }
}
