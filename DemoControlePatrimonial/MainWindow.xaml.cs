﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Threading;
using Ur2.RFID.Demo;
using Ur2.RFID.Middleware;
using Ur2.RFID.Middleware.Modelo;
using Ur2.Modelo;
using System.Collections.ObjectModel;

namespace Ur2.RFID.Demo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Bem> bens = new List<Bem>();
        private List<Local> locais = new List<Local>();
        private List<Pessoa> pessoas = new List<Pessoa>();
        private List<PadraoDepreciacao> padroes = new List<PadraoDepreciacao>();
        private List<Depreciacao> depreciacoes = new List<Depreciacao>();

        private List<ItemRelatorioInventario> relatorio = new List<ItemRelatorioInventario>();

        private MonitorPlanta planta = new MonitorPlanta();
        private MonitorCadeiaSuprimento lean = new MonitorCadeiaSuprimento();
        private MonitorEPI epi = new MonitorEPI();
        private MonitorPlantaRHB plantaRHB = new MonitorPlantaRHB();

        private bool inventariando = false;
        private Pessoa pessoaLogada;
        private Local localInventario;

        private IDevice dispositivoAtual;

        private string TAG_NAO_ENCONTRADO = "Nenhum tag encontrado. Tente novamente...";
        private string TAGLOCAL_NAO_ENCONTRADO = "Local não registrado. Tente novamente...";

        public MainWindow()
        {
            InitializeComponent();

            EnderecoMercuryTxb.Text = "";

            MudaDispositivoRFID();

            HabilitacaoBotoesMenu(false);
            AlteraVisibilidadeMenu(Visibility.Hidden);

            LoadAllXML();

            dispositivoAtual = DeviceFactory.CreateDevice(ConstantesDemoLocais.dispositivo);

            DeviceOpen();

            LoginFalso();
            //InserirDados();
        }

        private void LoadAllXML()
        {
            locais = XMLSerializerList.Load<Local>();
            pessoas = XMLSerializerList.Load<Pessoa>();
            padroes = XMLSerializerList.Load<PadraoDepreciacao>();
            bens = XMLSerializerList.Load<Bem>();

        }
        private void SaveAllXML()
        {
            XMLSerializerList.Save<Local>(locais);
            XMLSerializerList.Save<Pessoa>(pessoas);
            XMLSerializerList.Save<PadraoDepreciacao>(padroes);
            XMLSerializerList.Save<Bem>(bens);
        }

        #region Controle do Device
        private void DeviceOpen()
        {
            try
            {
                if (dispositivoAtual.Type == DeviceType.ThingMagicM6 && EnderecoMercuryTxb.Text.Trim().Length == 0)
                {
                    CaixaMensagem cm = new CaixaMensagem();
                    cm.DefineMensagem("Digite a url do leitor M6. Ex: 192.168.0.5.");
                    cm.ShowDialog();
                }
                dispositivoAtual = DeviceFactory.CreateDevice(ConstantesDemoLocais.dispositivo);
                if (!dispositivoAtual.Open(EnderecoMercuryTxb.Text))
                    return;

                DeviceAtivo();
                if (dispositivoAtual.Type == DeviceType.ThingMagicM6)
                    MostraEscondeEnderecoMercury(false);
            }
            catch (Exception e)
            {
                CaixaMensagem cm = new CaixaMensagem();
                cm.DefineMensagem("Erro ao tentear abrir a conexão: " + e.Message);
                cm.ShowDialog();
            }
        }
        private void DeviceClose()
        {
            if(dispositivoAtual!=null)
                dispositivoAtual.Close();
            DeviceInativo();
        }
        private void IniciaLeitura(bool continuo = false, bool limpamemoria = true)
        {
            dispositivoAtual.OnRead += new ChangeEventHandler(AtualizaMemoria);
            dispositivoAtual.StartReading(continuo, limpamemoria);
        }
        private void ParaLeitura()
        {
            dispositivoAtual.StopReading();
            dispositivoAtual.OnRead -= AtualizaMemoria;
        }
        private void DeviceAtivo()
        {
            DeviceLedAtivo.Visibility = Visibility.Visible;
        }
        private void DeviceInativo()
        {
            DeviceLedAtivo.Visibility = Visibility.Hidden;
        }
        private void MudaDispositivoRFID()
        {
            if (dispositivoAtual == null)
            {
                DefineDeviceDOTR900();
                return;
            }
            if (dispositivoAtual.IsActive)
                DeviceClose();
            if (ConstantesDemoLocais.dispositivo == DeviceType.DOTR900)
            {
                DefineDeviceM6();
                return;
            }
            DefineDeviceDOTR900();
        }
        private void DefineDeviceDOTR900()
        {
            ConstantesDemoLocais.dispositivo = DeviceType.DOTR900;
            DeviceLbl.Content = DeviceType.DOTR900.ToString();
            MostraEscondeEnderecoMercury(false);
        }
        private void DefineDeviceM6()
        {
            ConstantesDemoLocais.dispositivo = DeviceType.ThingMagicM6;
            DeviceLbl.Content = DeviceType.ThingMagicM6.ToString();
            MostraEscondeEnderecoMercury();
        }
        private void MostraEscondeEnderecoMercury(bool mostra = true)
        {
            if (!mostra)
            {
                EnderecoMercury.Visibility = Visibility.Hidden;
                return;
            }
            EnderecoMercury.Visibility = Visibility.Visible;
            EnderecoMercuryTxb.Focus();
        }
        #endregion Controle do Device
                
        #region Validacao
        private void BackgroundRed(Control obj)
        {
            SolidColorBrush sb = new SolidColorBrush();
            sb.Color = Colors.Red;
            obj.Background = sb;
        }

        private void BackgroundTransparent(Control obj)
        {
            SolidColorBrush sb = new SolidColorBrush();
            sb.Color = Colors.Transparent;
            obj.Background = sb;
        }

        private void BackgroundWhite(Control obj)
        {
            SolidColorBrush sb = new SolidColorBrush();
            sb.Color = Colors.White;
            obj.Background = sb;
        }

        private bool TemLinhaSelecionadaDataGrid(object sender, MouseButtonEventArgs e)
        {
            Object obj = e.OriginalSource;
            DependencyObject depo = ItemsControl.ContainerFromElement((DataGrid)sender, obj as DependencyObject);
            DataGridRow row = depo as DataGridRow;
            if (row == null || row.IsNewItem)
                return false;
            return true;
        }
        #endregion Validacao

        #region Bem
        private string ObtemSituacaoCombo()
        {
            if (BensSituacaoCbb.SelectedItem == null)
                return string.Empty;
            return BensSituacaoCbb.SelectedItem.ToString();
        }
        private string ObtemEstadoCombo()
        {
            if (BensEstadoCbb.SelectedItem == null)
                return string.Empty;
            return BensEstadoCbb.SelectedItem.ToString();
        }
        private void SalvaBem()
        {
            if (!ValidaCamposBem())
                return;

            if (Bem.ExisteBemIdTag(bens,BensTagTextbox.Text))
            {
                MessageBox("Já existe um bem cadastrado com este tag.");
                return;
            }

            // Coloca zero se valor for vazio
            decimal valoraux = decimal.TryParse(BensValorTextbox.Text.Trim(), out valoraux) ? valoraux : 0;

            bens.Add(new Bem(BensDescricaoTextbox.Text, BensDataDatetimePicker.DisplayDate, valoraux, BensTagTextbox.Text, ObtemSituacaoCombo(), ObtemEstadoCombo()));

            LimpaCamposBem();
        }
        private void IniciaLeituraTagBem()
        {
            ParaLeitura();
            BackgroundWhite(BensTagTextbox);
            AtualizaTagBemTxbThread();
            dispositivoAtual.OnRead += new ChangeEventHandler(AtualizaTagBem);
            IniciaLeitura();
        }
        private void ParaLeituraTagBem()
        {
            ParaLeitura();
            dispositivoAtual.OnRead -= AtualizaTagBem;
        }
        public void AtualizaTagBem()
        {
            ParaLeituraTagBem();
            if (!dispositivoAtual.Memory.isLastTagValid)
            {
                AtualizaTagBemTxbThread(TAG_NAO_ENCONTRADO);
                return;
            }
            AtualizaTagBemTxbThread(dispositivoAtual.Memory.LastTag.Id);
        }
        private void AtualizaTagBemTxb(string texto = "")
        {
            BensTagTextbox.Text = texto;
            BensTagTextbox.UpdateLayout();
        }
        private void AtualizaTagBemTxbThread(string texto = "")
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => AtualizaTagBemTxb(texto));
                return;
            }
            AtualizaTagBemTxb(texto);
        }
        private void AtualizaGridBens()
        {
            BensGrid.ItemsSource = bens;
            BensGrid.Items.Refresh();
        }
        private void LimpaCamposBem()
        {
            BensDescricaoTextbox.Text = string.Empty;
            BensDataDatetimePicker.DisplayDate = DateTime.Now;
            BensValorTextbox.Text = string.Empty;
            BensTagTextbox.Text = string.Empty;
        }
        private bool ValidaCamposBem()
        {
            DateTime dataaux;
            bool retorno = true;
            if (!DateTime.TryParse(BensDataDatetimePicker.DisplayDate.ToString(), out dataaux))
            {
                BackgroundRed(BensDataDatetimePicker);
                retorno = false;
            }
            else
            {
                BackgroundWhite(BensDataDatetimePicker);
            }

            if (BensDescricaoTextbox.Text.Trim() == string.Empty)
            {
                BackgroundRed(BensDescricaoTextbox);
                retorno = false;
            }
            else
            {
                BackgroundWhite(BensDescricaoTextbox);
            }

            if (BensTagTextbox.Text.Trim() == string.Empty || BensTagTextbox.Text == TAG_NAO_ENCONTRADO)
            {
                BackgroundRed(BensTagTextbox);
                retorno = false;
            }
            else
            {
                BackgroundWhite(BensTagTextbox);
            }
            if (ObtemSituacaoCombo().Length == 0)
            {
                retorno = false;
            }
            if (ObtemEstadoCombo().Length == 0)
            {
                retorno = false;
            }
            return retorno;
        }
        private Bem SelecionaBemDataGrid()
        {
            if (BensGrid.SelectedItem == null || !(BensGrid.SelectedItem is Bem))
                return null;
            return (Bem)BensGrid.SelectedItem;
        }
        private void RemoveBem()
        {
            bens.Remove((Bem)BensGrid.SelectedItem);
            AtualizaGridBens();
        }
        private void AbreTelaBens()
        {
            AtualizaGridBens();
            AtualizaComboPadroes();
            BensDataDatetimePicker.SelectedDate = DateTime.Now;
            BensCanvas.Visibility = Visibility.Visible;
        }
        private bool ExistemBemSelecionadoGrid()
        {
            if (SelecionaBemDataGrid() == null)
            {
                return false;
            }
            return true;
        }

        #region Inventarios
        private void AtualizaGridHistoricoInventarios(List<Inventario> inventarios)
        {
            ListaInventarioDatagrid.ItemsSource = inventarios;
            ListaInventarioDatagrid.Items.Refresh();
        }
        private void AbreTelaHistoricoInvetarios()
        {
            Bem item = SelecionaBemDataGrid();
            AtualizaGridHistoricoInventarios(item.ObtemInventariosOrdenado());
            HistoricoInventarioCanvas.Visibility = Visibility.Visible;
        }
        private void ListaInventarioFecharBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            HistoricoInventarioCanvas.Visibility = Visibility.Hidden;
        }
        #endregion Inventarios

        #region AssociacaoPadraoDepreciacao
        private string cbselecionadoassociacaopadrao = string.Empty;
        const string primeiroitemcomboassociacaopadrao = "Selecione....";
        private void AtualizaComboPadroes()
        {
            ObservableCollection<string> source = new ObservableCollection<string>();
            source.Add(primeiroitemcomboassociacaopadrao);
            foreach (PadraoDepreciacao item in padroes)
            {
                source.Add(item.Nome);
            }
            PadraoDepreciacaoCbb.ItemsSource = source;
            PadraoDepreciacaoCbb.SelectedIndex = 0;
        }
        private void AbreTelaAssociacaoPadraoDepreciacao()
        {
            if (!ExistemBemSelecionadoGrid())
            {
                MessageBox("Selecione um bem da lista.");
                return;
            }
            AtualizaComboPadroes();
            AssociarPadraoCanvas.Visibility = Visibility.Visible;
        }
        private void AlteraPadraoDepreciacaoBem()
        {
            if (cbselecionadoassociacaopadrao.Trim().Length == 0 || cbselecionadoassociacaopadrao.Trim() == primeiroitemcomboassociacaopadrao)
            {
                MessageBox("Padrão inválido.");
                return;
            }
            Bem bem = SelecionaBemDataGrid();
            PadraoDepreciacao pad = padroes.Find(x => x.Nome == cbselecionadoassociacaopadrao);
            if (pad == null)
            {
                MessageBox("Padrão não encontrado.");
                return;
            }
            bem.AdicionaPadraoDepreciacao(pad);
        }

        #region eventos
        private void AssociarPadraoFecharBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            AlteraPadraoDepreciacaoBem();
            AtualizaGridBens();
            AssociarPadraoCanvas.Visibility = Visibility.Hidden;
        }
        private void BensAssociarPadraoBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            AbreTelaAssociacaoPadraoDepreciacao();
        }
        private void PadraoDepreciacaoCbb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cbselecionadoassociacaopadrao = ((sender as ComboBox).SelectedItem as string);
        }
        #endregion eventos
        #endregion AssociacaoPadraoDepreciacao

        #region HistoricoDepreciacao
        private void AtualizaGridHistoricoDepreciacoes(List<Depreciacao> depreciacoes)
        {
            ListaDepreciacaoDatagrid.ItemsSource = depreciacoes;
            ListaDepreciacaoDatagrid.Items.Refresh();
        }
        private void AbreTelaHistoricoDepreciacoes(List<Depreciacao> depreciacoes)
        {
            if (!ExistemBemSelecionadoGrid())
            {
                MessageBox("Selecione um bem da lista.");
                return;
            }
            AtualizaGridHistoricoDepreciacoes(depreciacoes);
            HistoricoDepreciacaoCanvas.Visibility = Visibility.Visible;
        }
        #region eventos
        private void ListaDepreciacaoFecharBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            HistoricoDepreciacaoCanvas.Visibility = Visibility.Hidden;
        }
        private void BensHistoricoDepeciacaoBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Bem item = SelecionaBemDataGrid();
            if (item == null)
            {
                MessageBox("Selecione um bem na lista para ver as depreciações.");
                return;
            }
            AbreTelaHistoricoDepreciacoes(item.ObtemDepreciacoesOrdenado());
        }
        #endregion eventos
        #endregion HistoricoDepreciacao

        #region HistoricoInventario
        private void BensHistoricoInventarioBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (!ExistemBemSelecionadoGrid())
            {
                MessageBox("Selecione um bem da lista.");
                return;
            }

            AbreTelaHistoricoInvetarios();
        }
        #endregion HistoricoInventario

        #region eventos
        private void BensAdicionaBt_MouseDown(object sender, MouseButtonEventArgs e)
        {
            SalvaBem();
            AtualizaGridBens();
        }
        private void BensDescricaoTextbox_GotFocus(object sender, RoutedEventArgs e)
        {
            BackgroundWhite(BensDescricaoTextbox);
        }
        private void BensDescricaoTextbox_LostFocus(object sender, RoutedEventArgs e)
        {
            BensDescricaoTextbox.Text = BensDescricaoTextbox.Text.Trim();
        }
        private void BensTagTextbox_GotFocus(object sender, RoutedEventArgs e)
        {
            IniciaLeituraTagBem();
        }
        private void BemFecharBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            BensCanvas.Visibility = Visibility.Hidden;
        }
        private void BensLeituraBt_MouseDown(object sender, MouseButtonEventArgs e)
        {
            IniciaLeituraTagBem();
        }
        private void BensBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            AbreTelaBens();
        }
        private void BensGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
                RemoveBem();
        }
        private void BensEstadoCbb_Loaded(object sender, RoutedEventArgs e)
        {
            // ... A List.
            List<string> data = new List<string>();
            data.Add("Ruim");
            data.Add("Novo");
            data.Add("Inservivel");
            data.Add("Servivel");

            // ... Get the ComboBox reference.
            var comboBox = sender as ComboBox;

            // ... Assign the ItemsSource to the List.
            comboBox.ItemsSource = data;

            // ... Make the first item selected.
            comboBox.SelectedIndex = 0;
        }
        private void BensSituacaoCbb_Loaded(object sender, RoutedEventArgs e)
        {
            // ... A List.
            List<string> data = new List<string>();
            data.Add("Em uso");
            data.Add("Disponível");
            data.Add("Indisponível");
            data.Add("Extraviado");

            // ... Get the ComboBox reference.
            var comboBox = sender as ComboBox;

            // ... Assign the ItemsSource to the List.
            comboBox.ItemsSource = data;

            // ... Make the first item selected.
            comboBox.SelectedIndex = 0;
        }
        #endregion eventos

        #endregion Bem
        
        #region Inventario
        private void IniciaLeituraInventario()
        {
            ParaLeituraInventario(false);
            dispositivoAtual.OnRead += new ChangeEventHandler(AtualizaGridInventarioThread);
            inventariando = true;
            IniciaLeitura(true);
        }
        private void ParaLeituraInventario(bool registraInventario = true)
        {
            ParaLeitura();
            inventariando = false;
            dispositivoAtual.OnRead -= new ChangeEventHandler(AtualizaGridInventarioThread);
            if(registraInventario)
                RegistrarInventario();
        }
        public void RegistrarInventario()
        {
            if (inventariando)
                return;

            foreach (RFIDTag item in dispositivoAtual.Memory.Tags)
            {
                AdicionaInventarioBem(item.Id);
            }

            AtualizaGridBens();
        }
        private void BuscaLocalInventario()
        {
            ParaLeitura();
            dispositivoAtual.OnRead += new ChangeEventHandler(AtualizaLocalInventario);
            ColetaInventarioLocalSelecionadoLbl.Content = "";
            IniciaLeitura();
            Thread.SpinWait(500);
            if (ColetaInventarioLocalSelecionadoLbl.Content.ToString().Length == 0)
                ColetaInventarioLocalSelecionadoLbl.Content = TAG_NAO_ENCONTRADO;
        }
        private void AtualizaLocalInventario()
        {
            ParaLeitura();
            dispositivoAtual.OnRead -= new ChangeEventHandler(AtualizaLocalInventario);
            if (!dispositivoAtual.Memory.isLastTagValid)
                return;

            localInventario = EncontraLocal(dispositivoAtual.Memory.LastTag.Id);

            if (EncontraLocalInventario())
                IniciaLeituraInventario();
        }
        private void AtualizaGridInventario()
        {
            ColetaInventarioDataGrid.ItemsSource = dispositivoAtual.Memory.Tags;
            ColetaInventarioDataGrid.Items.Refresh();
        }
        private void AtualizaGridInventarioThread()
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => AtualizaGridInventario());
                return;
            }
            AtualizaGridInventario();
        }
        private bool EncontraLocalInventario()
        {
            if (localInventario == null)
            {
                AtualizaLocalInventarioLblThread(TAGLOCAL_NAO_ENCONTRADO);
                return false;
            }
            AtualizaLocalInventarioLblThread(localInventario.Nome);

            return true;
        }
        private void AtualizaLocalInventarioLbl(string texto = "")
        {
            ColetaInventarioLocalSelecionadoLbl.Content = texto;
            ColetaInventarioLocalSelecionadoLbl.UpdateLayout();
        }
        private void AtualizaLocalInventarioLblThread(string texto = "")
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => AtualizaLocalInventarioLbl(texto));
                return;
            }
            AtualizaLocalInventarioLbl(texto);
        }
        private void AdicionaInventarioBem(string tagIdBem)
        {
            //Valida assertivas
            if (tagIdBem == null || tagIdBem.Trim().Length == 0)
                return;
            if (localInventario == null || pessoaLogada == null)
                return;
            if (!localInventario.souValido() || !pessoaLogada.souValida())
                return;

            if (Bem.EncontraBemIdTag(bens,tagIdBem) != null)
            {
                Bem.EncontraBemIdTag(bens, tagIdBem).ObtemInventarios().Add(new Inventario(localInventario, pessoaLogada));
            }
        }
        private void FechaTelaInvetario()
        {
            ParaLeituraInventario(false);
            dispositivoAtual.Memory.Clear();
            ColetaInventarioCanvas.Visibility = Visibility.Hidden;
        }

        #region eventos
        private void ColetaInventarioAtivaAntenaBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            BuscaLocalInventario();
        }
        private void ColetaInventarioDesativaAntenaBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ParaLeituraInventario();
        }
        private void ColetaInventarioBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ColetaInventarioCanvas.Visibility = Visibility.Visible;
        }
        private void ColetaInventarioFecharBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            FechaTelaInvetario();
        }
        #endregion eventos
        #endregion Inventario

        #region RelatorioInventario

        private void ImprimeDados()
        {
            CaixaMensagem cx = new CaixaMensagem();
            cx.DefineMensagem("Em desenvolvimento...");
            cx.Show();
            
        }

        private void ExportarExcel()
        {
            RelatorioInventarioDataGrid.SelectAllCells();
            RelatorioInventarioDataGrid.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
            ApplicationCommands.Copy.Execute(null, RelatorioInventarioDataGrid);
            String resultat = (string)Clipboard.GetData(DataFormats.CommaSeparatedValue);
            String result = (string)Clipboard.GetData(DataFormats.Text);
            RelatorioInventarioDataGrid.UnselectAllCells();
            string path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            System.IO.StreamWriter file = new System.IO.StreamWriter(path + "\\RelatorioBens.xls");
            file.WriteLine(result.Replace(',', ' '));
            file.Close();

            MessageBox("Exportado");
        }

        private void CarregaRelatorio()
        {
            if (bens == null || bens.Count == 0)
                return;

            relatorio.Clear();
            
            RelatorioInventarioDataGrid.ItemsSource = ItemRelatorioInventario.MontaRelatorio(bens);
            RelatorioInventarioDataGrid.Items.Refresh();
        }
        
        #region eventos
        private void RelatorioInventarioFecharBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            RelatorioInventarioCanvas.Visibility = Visibility.Hidden;
        }

        private void RelatorioInventarioExportarBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ExportarExcel();
        }
        private void InventarioRelatorioBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            CarregaRelatorio();
            RelatorioInventarioCanvas.Visibility = Visibility.Visible;
        }
        #endregion eventos

        #endregion RelatorioInventario

        #region MapaGL
        private int qtdponteiros = 0;
        private void CriaPonteiroMapa(double x, double y)
        {
            qtdponteiros++;
            Image i = new Image();
            i.Width = 20;
            i.Height = 35;
            i.Name = "MapaGLPointer" + qtdponteiros.ToString();
            i.Uid = i.Name;
            i.Source = new System.Windows.Media.Imaging.BitmapImage(new Uri("img/icones/pointer.png", UriKind.Relative));

            PontosMapaCanvas.Children.Add(i);
            Canvas.SetTop(i, x);
            Canvas.SetLeft(i,y);
        }
        private void ClearCanvas()
        {
            PontosMapaCanvas.Children.RemoveRange(0, PontosMapaCanvas.Children.Count);
        }
        private void AbreMapaGL()
        {
            ClearCanvas();
            foreach (Bem item in bens)
            {
                if (item.ObtemInventariosOrdenado().Count == 0)
                    continue;
                PontoMapa ponto = item.ObtemInventariosOrdenado()[0].ObtemPontoMapa();
                CriaPonteiroMapa(415-(ponto.Latitude*4.6), 335-(ponto.Longitude*3.7));
            }
        }

        #region eventos
        private void MapaGLFecharBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            MapaGLCanvas.Visibility = Visibility.Hidden;
        }

        private void MapaGLBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            AbreMapaGL();
            MapaGLCanvas.Visibility = Visibility.Visible;
        }
        #endregion eventos
        #endregion MapaGL

        #region Usuario
        private void IniciaLeituraUsuarioTag()
        {
            ParaLeitura();
            dispositivoAtual.OnRead += new ChangeEventHandler(AtualizaUsuarioTag);
            IniciaLeitura();
        }
        private void ParaLeituraUsuarioTag()
        {
            ParaLeitura();
            dispositivoAtual.OnRead -= AtualizaUsuarioTag;
        }
        private void AtualizaUsuarioTag()
        {
            ParaLeituraUsuarioTag();
            if (!dispositivoAtual.Memory.isLastTagValid)
            {
                return;
            }

            List<Pessoa> pes = Pessoa.EncontraPessoasListaTag(pessoas, dispositivoAtual.Memory.Tags);
            if(pes.Count > 0)
            {
                pessoaLogada = pes[0];
                ValidaPessoaLogada();                
            }
        }
        private void ValidaPessoaLogada()
        {
            if (pessoaLogada == null)
            {
                Logout();
                return;
            }
            ValidaPessoaLogadaTxbThread();
            Login();
        }
        private void ValidaPessoaLogadaTxb()
        {
            UsuarioSelecionadoLbl.Content = pessoaLogada.Nome;
            UsuarioSelecionadoLbl.UpdateLayout();
        }
        private void ValidaPessoaLogadaTxbThread()
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => ValidaPessoaLogadaTxb());
                return;
            }
            ValidaPessoaLogadaTxb();
        }
        private void Login()
        {
            AlteraVisibilidadeMenuThread();
            HabilitacaoBotoesMenuThread();
        }
        private void Logout()
        {
            pessoaLogada = null;
            localInventario = null;
            UsuarioSelecionadoLbl.Content = string.Empty;
            UsuarioSelecionadoLbl.UpdateLayout();
            HabilitacaoBotoesMenuThread(false);
            AlteraVisibilidadeMenuThread(Visibility.Hidden);
        }
        private void HabilitacaoBotoesMenu(bool habilita = true)
        {
            BensBt.IsEnabled = habilita;
            ColetaInventarioBt.IsEnabled = habilita;
            LocalBt.IsEnabled = habilita;
            PadraoDepreciacaoBt.IsEnabled = habilita;
            DepreciarBt.IsEnabled = habilita;
            InventarioRelatorioBt.IsEnabled = habilita;
            MapaGLBt.IsEnabled = habilita;
            PlantaRHBBt.IsEnabled = habilita;
            MonitoriaBt.IsEnabled = habilita;
            //MapaBt.IsEnabled = habilita;
            LeanBt.IsEnabled = habilita;
            EPIBt.IsEnabled = habilita;
        }
        private void HabilitacaoBotoesMenuThread(bool habilita = true)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => HabilitacaoBotoesMenu(habilita));
                return;
            }
            HabilitacaoBotoesMenu(habilita);
        }
        private void AlteraVisibilidadeMenu(Visibility visibilidade = Visibility.Visible)
        {
            BensBt.Visibility = visibilidade;
            ColetaInventarioBt.Visibility = visibilidade;
            LocalBt.Visibility = visibilidade;
            PadraoDepreciacaoBt.Visibility = visibilidade;
            DepreciarBt.Visibility = visibilidade;
            InventarioRelatorioBt.Visibility = visibilidade;
            MapaGLBt.Visibility = visibilidade;
            PlantaRHBBt.Visibility = visibilidade;
            MonitoriaBt.Visibility = visibilidade;
            //MapaBt.Visibility = visibilidade;
            LeanBt.Visibility = visibilidade;
            EPIBt.Visibility = visibilidade;
        }
        private void AlteraVisibilidadeMenuThread(Visibility visibilidade = Visibility.Visible)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => AlteraVisibilidadeMenu(visibilidade));
                return;
            }
            AlteraVisibilidadeMenu(visibilidade);
        }

        #region eventos
        private void UsuarioLoginBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            IniciaLeituraUsuarioTag();
        }
        #endregion eventos

        #endregion Usuario

        #region Pessoa
        private void IniciaLeituraTagPessoa()
        {
            ParaLeitura();
            BackgroundWhite(PessoaTagTextbox);
            AlteraPessoaTagTxbThread();
            dispositivoAtual.OnRead += new ChangeEventHandler(AtualizaTagPessoa);
            IniciaLeitura();
        }
        private void ParaLeituraTagPessoa()
        {
            ParaLeitura();
            dispositivoAtual.OnRead -= new ChangeEventHandler(AtualizaTagPessoa);
        }
        public void AtualizaTagPessoa()
        {
            ParaLeituraTagPessoa();
            if (!dispositivoAtual.Memory.isLastTagValid)
            {
                AlteraPessoaTagTxbThread(TAG_NAO_ENCONTRADO);
                return;
            }
            AlteraPessoaTagTxbThread(dispositivoAtual.Memory.LastTag.Id);
        }
        private void AlteraPessoaTagTxbThread(string texto = "")
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => AlteraPessoaTagTxb(texto));
                return;
            }
            AlteraPessoaTagTxb(texto);
        }
        private void AlteraPessoaTagTxb(string texto = "")
        {
            PessoaTagTextbox.Text = texto;
            PessoaTagTextbox.UpdateLayout();
        }
        private void SalvaPessoa()
        {
            if (!ValidaCamposPessoa())
                return;

            if (Pessoa.ExistePessoaIdTag(pessoas, PessoaTagTextbox.Text))
            {
                MessageBox("Já existe uma pessoa cadastrada com este tag.");
                return;
            }
            try
            {
                pessoas.Add(new Pessoa(PessoaNomeTextbox.Text, PessoaTagTextbox.Text));
            }
            catch (ArgumentOutOfRangeException ex)
            {
                BackgroundRed(LocalLatitudeTextbox);
                BackgroundRed(LocalLongitudeTextbox);
                MessageBox(ex.Message);
                return;
            }

            LimpaCamposPessoa();
        }
        private void LimpaCamposPessoa()
        {
            PessoaNomeTextbox.Text = string.Empty;
            PessoaTagTextbox.Text = string.Empty;
        }
        private bool ValidaCamposPessoa()
        {
            bool retorno = true;

            if (PessoaNomeTextbox.Text.Trim().Length == 0)
            {
                BackgroundRed(PessoaNomeTextbox);
                retorno = false;
            }
            else
            {
                BackgroundWhite(PessoaNomeTextbox);
            }

            if (PessoaTagTextbox.Text.Trim().Length == 0 || PessoaTagTextbox.Text == TAG_NAO_ENCONTRADO)
            {
                BackgroundRed(PessoaTagTextbox);
                retorno = false;
            }
            else
            {
                BackgroundWhite(PessoaTagTextbox);
            }

            return retorno;
        }
        private Pessoa SelecionaPessoaDataGrid(object sender, MouseButtonEventArgs e)
        {
            if (!TemLinhaSelecionadaDataGrid(sender, e))
                return null;
            return (Pessoa)PessoasGrid.SelectedItem;
        }
        private void RemovePessoa()
        {
            pessoas.Remove((Pessoa)LocaisGrid.SelectedItem);
            PessoasGrid.Items.Refresh();
        }
        private void AbreTelaPessoas()
        {
            PessoasGrid.ItemsSource = pessoas;
            PessoasCanvas.Visibility = Visibility.Visible;
        }

        #region eventos
        private void PessoaFecharBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            PessoasCanvas.Visibility = Visibility.Hidden;
        }
        private void PessoasGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Pessoa item = SelecionaPessoaDataGrid(sender, e);
        }
        private void PessoaNomeTextbox_GotFocus(object sender, RoutedEventArgs e)
        {
            BackgroundWhite(PessoaNomeTextbox);
        }
        private void PessoaNomeTextbox_LostFocus(object sender, RoutedEventArgs e)
        {
            PessoaNomeTextbox.Text = PessoaNomeTextbox.Text.Trim();
        }
        private void PessoaTagTextbox_GotFocus(object sender, RoutedEventArgs e)
        {
            IniciaLeituraTagPessoa();
        }
        private void PessoaAdicionaBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            SalvaPessoa();
            PessoasGrid.Items.Refresh();
        }
        private void PessoaLeituraBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            IniciaLeituraTagPessoa();
        }
        private void PessoaBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            AbreTelaPessoas();
        }
        #endregion eventos

        #endregion Pessoa

        #region Locais
        private Local EncontraLocal(string tagid)
        {
            return locais.Find(item => item.Tag.Id == tagid);
        }
        private bool ExisteLocal(string tagid)
        {
            Local aux = EncontraLocal(tagid);

            if (aux != null)
            {
                return true;
            }
            return false;
        }
        private List<Local> EncontraLocais(string tagid)
        {
            return locais.FindAll(item => item.Tag.Id == tagid);
        }
        private void IniciaLeituraTagLocal()
        {
            ParaLeitura();
            BackgroundWhite(LocalTagTextbox);
            AtualizaTagLocalTxbThread();
            dispositivoAtual.OnRead += new ChangeEventHandler(AtualizaTagLocal);
            IniciaLeitura();
        }
        private void ParaLeituraTagLocal()
        {
            ParaLeitura();
            dispositivoAtual.OnRead -= AtualizaTagLocal;
        }
        public void AtualizaTagLocal()
        {
            ParaLeituraTagLocal();
            if (!dispositivoAtual.Memory.isLastTagValid)
            {
                AtualizaTagLocalTxbThread(TAG_NAO_ENCONTRADO);
                return;
            }
            AtualizaTagLocalTxbThread(dispositivoAtual.Memory.LastTag.Id);
        }
        private void AtualizaTagLocalTxb(string texto = "")
        {
            LocalTagTextbox.Text = texto;
            LocalTagTextbox.UpdateLayout();
        }
        private void AtualizaTagLocalTxbThread(string texto = "")
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => AtualizaTagLocalTxb(texto));
                return;
            }
            AtualizaTagLocalTxb(texto);
        }
        private void SalvaLocal()
        {
            if (!ValidaCamposLocal())
                return;

            if (ExisteLocal(LocalTagTextbox.Text))
            {
                MessageBox("Já existe um local cadastrado com este tag.");
                return;
            }
            double latitude = 0;
            double longitude = 0;
            if(!Double.TryParse(LocalLatitudeTextbox.Text,out latitude) || !Double.TryParse(LocalLongitudeTextbox.Text, out longitude))
                return;
            try
            {
                locais.Add(new Local(LocalNomeTextbox.Text, LocalTagTextbox.Text, new PontoMapa(latitude, longitude)));
            }
            catch (ArgumentOutOfRangeException ex)
            {
                BackgroundRed(LocalLatitudeTextbox);
                BackgroundRed(LocalLongitudeTextbox);
                MessageBox(ex.Message);
                return;
            }

            LimpaCamposLocal();
        }
        private void LimpaCamposLocal()
        {
            LocalNomeTextbox.Text = string.Empty;
            LocalTagTextbox.Text = string.Empty;
            LocalLatitudeTextbox.Text = string.Empty;
            LocalLongitudeTextbox.Text = string.Empty;
        }
        private bool ValidaCamposLocal()
        {
            bool retorno = true;

            if (LocalNomeTextbox.Text.Trim().Length == 0)
            {
                BackgroundRed(LocalNomeTextbox);
                retorno = false;
            }
            else
            {
                BackgroundWhite(LocalNomeTextbox);
            }

            if (LocalLatitudeTextbox.Text.Trim().Length == 0)
            {
                BackgroundRed(LocalLatitudeTextbox);
                retorno = false;
            }
            else
            {
                BackgroundWhite(LocalLatitudeTextbox);
            }

            if (LocalLongitudeTextbox.Text.Trim().Length == 0)
            {
                BackgroundRed(LocalLongitudeTextbox);
                retorno = false;
            }
            else
            {
                BackgroundWhite(LocalLongitudeTextbox);
            }

            if (LocalTagTextbox.Text.Trim().Length == 0 || LocalTagTextbox.Text == TAG_NAO_ENCONTRADO)
            {
                BackgroundRed(LocalTagTextbox);
                retorno = false;
            }
            else
            {
                BackgroundWhite(LocalTagTextbox);
            }

            return retorno;
        }
        private Local SelecionaLocalDataGrid(object sender, MouseButtonEventArgs e)
        {
            if (!TemLinhaSelecionadaDataGrid(sender, e))
                return null;
            return (Local)LocaisGrid.SelectedItem;
        }
        private void RemoveLocal()
        {
            locais.Remove((Local)LocaisGrid.SelectedItem);
            LocaisGrid.Items.Refresh();
        }
        private void AbreTelaLocais()
        {
            LocaisGrid.ItemsSource = locais;
            LocaisCanvas.Visibility = Visibility.Visible;
        }

        #region eventos
        private void LocaisFecharBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            LocaisCanvas.Visibility = Visibility.Hidden;
        }
        private void LocalNomeTextbox_GotFocus(object sender, RoutedEventArgs e)
        {
            BackgroundWhite(LocalNomeTextbox);
        }
        private void LocalNomeTextbox_LostFocus(object sender, RoutedEventArgs e)
        {
            LocalNomeTextbox.Text = LocalNomeTextbox.Text.Trim();
        }
        private void LocalTagTextbox_GotFocus(object sender, RoutedEventArgs e)
        {
            IniciaLeituraTagLocal();
        }
        private void LocalAdicionaBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            SalvaLocal();
            LocaisGrid.Items.Refresh();
        }
        private void LocalLeituraBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            IniciaLeituraTagLocal();
        }
        private void LocalBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            AbreTelaLocais();
        }
        private void LocalLatitudeTextbox_GotFocus(object sender, RoutedEventArgs e)
        {
            BackgroundWhite(LocalLatitudeTextbox);
        }
        private void LocalLongitudeTextbox_GotFocus(object sender, RoutedEventArgs e)
        {
            BackgroundWhite(LocalLongitudeTextbox);
        }
        #endregion eventos

        #endregion Locais

        #region Depreciar
        private void AbreTelaDepreciar()
        {
            DepreciarDataGrid.ItemsSource = depreciacoes;
            DepreciarCanvas.Visibility = Visibility.Visible;
        }
        private void AtualizarGridDepreciacoes()
        {
            DepreciarDataGrid.Items.Refresh();
        }
        private void ExecutarDepreciacao(bool avaliarDataCiclo = true)
        {
            foreach (Bem item in bens)
            {
                if (item.ObtemPadraoDepreciacao() == null || item.ObtemPadraoDepreciacao().Nome == string.Empty)
                    continue;
                DateTime ultimaDepreciacao = item.MaiorDataDepreciacao();
                if (avaliarDataCiclo & ((ultimaDepreciacao.AddMonths(item.ObtemPadraoDepreciacao().Ciclomeses) - DateTime.Today).Days >= 0))
                    continue;
                Depreciacao dep = new Depreciacao();
                dep.PercentualAplicado = item.ObtemPadraoDepreciacao().Percentual;
                dep.ValorOriginal = item.ValorAtual;
                decimal perc = item.ObtemPadraoDepreciacao().Percentual;
                dep.ValorDepreciado = item.ValorAtual * (perc / 100);
                item.ValorAtual = item.ValorAtual - dep.ValorDepreciado;
                item.ObtemDepreciacoes().Add(dep);
                depreciacoes.Add(dep);
            }
        }

        #region eventos
        private void DepreciarBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            AbreTelaDepreciar();
        }
        private void ExecutarDepreciacaoBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            bool aux = false;
            bool? avaliarDataCiclo = DepreciarValidarCicloChk.IsChecked;
            if (avaliarDataCiclo == null || avaliarDataCiclo == true)
                aux = true;
            else
                aux = false;
            ExecutarDepreciacao(aux);
            AtualizarGridDepreciacoes();
            MessageBox("Calculo depreciação finalizado.");
        }
        private void DepreciarFecharBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            DepreciarCanvas.Visibility = Visibility.Hidden;
        }
        #endregion eventos
        #endregion Depreciar

        #region PadraoDepreciacao
        private void AbreTelaPadroesDepreciacao()
        {
            PadroesGrid.ItemsSource = padroes;
            PadroesDepreciacaoCanvas.Visibility = Visibility.Visible;
        }
        private bool ValidaCamposPadraoDepreciacao()
        {
            bool retorno = true;

            BackgroundWhite(PadraoNomeTextbox);
            if (PadraoNomeTextbox.Text.Trim().Length == 0)
            {
                BackgroundRed(PadraoNomeTextbox);
                retorno = false;
            }

            BackgroundWhite(PadraoCicloMesesTextbox);
            if (PadraoCicloMesesTextbox.Text.Trim().Length == 0)
            {
                BackgroundRed(PadraoCicloMesesTextbox);
                retorno = false;
            }

            BackgroundWhite(PadraoPercentualCicloTextbox);
            if (PadraoPercentualCicloTextbox.Text.Trim().Length == 0)
            {
                BackgroundRed(PadraoPercentualCicloTextbox);
                retorno = false;
            }

            return retorno;
        }
        private PadraoDepreciacao EncontraPadraoDepreciacao(string nome)
        {
            return padroes.Find(item => item.Nome == nome);
        }
        private bool ExistePadraoDepreciacao(string nome)
        {
            PadraoDepreciacao aux = EncontraPadraoDepreciacao(nome);

            if (aux != null)
            {
                return true;
            }
            return false;
        }
        private void LimpaCamposPadraoDepreciacao()
        {
            PadraoNomeTextbox.Text = string.Empty;
            PadraoCicloMesesTextbox.Text = string.Empty;
            PadraoPercentualCicloTextbox.Text = string.Empty;
        }
        private void SalvaPadraoDepreciacao()
        {
            if (!ValidaCamposPadraoDepreciacao())
                return;

            if (ExistePadraoDepreciacao(PadraoNomeTextbox.Text))
            {
                MessageBox("Já existe um local cadastrado com este nome.");
                return;
            }

            int ciclo = 0;
            int percentual = 0;
            if (!Int32.TryParse(PadraoCicloMesesTextbox.Text, out ciclo))
            {
                BackgroundRed(PadraoCicloMesesTextbox);
                MessageBox("Valor do ciclo é inválido.");
                return;
            }

            if (!Int32.TryParse(PadraoPercentualCicloTextbox.Text, out percentual))
            {
                BackgroundRed(PadraoPercentualCicloTextbox);
                MessageBox("Valor do percentual é inválido.");
                return;
            }

            try
            {
                padroes.Add(new PadraoDepreciacao(PadraoNomeTextbox.Text, ciclo, percentual));
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox(ex.Message);
                return;
            }

            LimpaCamposPadraoDepreciacao();
        }
        private PadraoDepreciacao SelecionaPadraoDataGrid(object sender, MouseButtonEventArgs e)
        {
            if (!TemLinhaSelecionadaDataGrid(sender, e))
                return null;
            return (PadraoDepreciacao)PadroesGrid.SelectedItem;
        }
        private void RemovePadraoDepreciacao()
        {
            padroes.Remove((PadraoDepreciacao)PadroesGrid.SelectedItem);
            PadroesGrid.Items.Refresh();
        }

        #region eventos
        private void PadraoDepreciacaoBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            AbreTelaPadroesDepreciacao();
        }
        private void PadraoNomeTextbox_GotFocus(object sender, RoutedEventArgs e)
        {
            BackgroundWhite(PadraoNomeTextbox);
        }
        private void CicloMesesTextbox_GotFocus(object sender, RoutedEventArgs e)
        {
            BackgroundWhite(PadraoCicloMesesTextbox);
        }
        private void PercentualCicloTextbox_GotFocus(object sender, RoutedEventArgs e)
        {
            BackgroundWhite(PadraoPercentualCicloTextbox);
        }
        private void PadraoAdicionaBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            SalvaPadraoDepreciacao();
            PadroesGrid.Items.Refresh();
        }
        private void PadraoFecharBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            PadroesDepreciacaoCanvas.Visibility = Visibility.Hidden;
        }
        #endregion eventos

        #endregion PadraoDepreciacao

        #region AbreTelas
        private void AbreTelaPlanta()
        {
            planta.DefineLocais(locais);
            planta.Show();
        }
        private void AbreTelaPlantaRHB()
        {
            plantaRHB.DefineLocais(locais);
            plantaRHB.Show();
        }
        private void AbreTelaSuprimento()
        {
            lean.DefineLocais(locais);
            lean.Show();
        }
        private void AbreTelaEPIObras()
        {
            epi.DefinePessoas(pessoas);
            epi.DefineBens(bens);
            epi.DefineLocais(locais);
            epi.Show();
        }
        #endregion AbreTelas

        #region Eventos
        
        #region Monitor
        private void MapaBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            
        }
        private void LeanBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            AbreTelaSuprimento();
        }
        private void EPIBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            AbreTelaEPIObras();
        }
        private void MonitoriaBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            AbreTelaPlanta();
        }
        private void PlantaRHBBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            AbreTelaPlantaRHB();
        }
        #endregion Monitor

        private void CanExecute_Open(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        private void Executed_Open(object sender, ExecutedRoutedEventArgs e)
        {
            //AbreDebug();
            CaixaMensagem msg = new CaixaMensagem();
            msg.DefineMensagem(dispositivoAtual.Memory.GetMessages().ToString());
            msg.Show();
        }
        private void DebugCanvasFechaBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            DebugCanvas.Visibility = Visibility.Hidden;
        }
        private void DeviceLedAtivo_MouseUp(object sender, MouseButtonEventArgs e)
        {
            DeviceClose();
        }
        private void DeviceLed_MouseUp(object sender, MouseButtonEventArgs e)
        {
            DeviceOpen();
        }
        private void SairBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            DeviceClose();
            Application.Current.Shutdown();
            Environment.Exit(0);
        }
        private void MoveBt_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MoveTela(sender, e);
        }
        private void MoveTela(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
        private void VolumeHigh_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try 
            {
                dispositivoAtual.BuzzVolume = BuzzVolume.High;
            }
            catch (NotImplementedException ex)
            {
                CaixaMensagem cm = new CaixaMensagem();
                cm.DefineMensagem(ex.Message);
                cm.ShowDialog();
            }

        }
        private void VolumeMute_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                dispositivoAtual.BuzzVolume = BuzzVolume.Mute;
            }
            catch (NotImplementedException ex)
            {
                CaixaMensagem cm = new CaixaMensagem();
                cm.DefineMensagem(ex.Message);
                cm.ShowDialog();
            }
        }
        private void ChangeDevice_MouseUp(object sender, MouseButtonEventArgs e)
        {
            MudaDispositivoRFID();
        }
        private void SalvarBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            SaveAllXML();
            MessageBox("Dados salvos");
        }
        #endregion Eventos

        private void MessageBox(string text)
        {
            CaixaMensagem cm = new CaixaMensagem();
            cm.DefineMensagem(text);
            cm.ShowDialog();
        }
        
        #region DadosBasicosDemo
        private void InserirDados()
        {
            //InserirBens();
            //InserirPessoas();
            //InserirLocais();
            InserirInventario();
        }

        private void InserirBens()
        {
            bens.Add(new Bem("Capacete", DateTime.Now, 0, "000000000000000000002207"));
            bens.Add(new Bem("Luva", DateTime.Now, 0, "000000000000000000002205"));
            bens.Add(new Bem("Cracha", DateTime.Now, 0, "000000F43474213000020143"));
            bens.Add(new Bem("cinto", DateTime.Now, 0, "000000000000000000100047"));

            XMLSerializerList.Save<Bem>(bens);
        }

        private void InserirPessoas()
        {
            pessoas.Add(new Pessoa("Mauricio", "10014D000000000000007557"));
            pessoas.Add(new Pessoa("Marcelo", "10014D000000000000007557"));
            pessoas.Add(new Pessoa("Pedro", "10014D000000000000007557"));

            XMLSerializerList.Save<Pessoa>(pessoas);
        }

        private void InserirLocais()
        {
            locais.Add(new Local("EstoqueMateriaPrima", "000000000000000000100047",new PontoMapa(-22.9015,-43.1819)));
            locais.Add(new Local("Setor1", "000000F43474213000020143", new PontoMapa(-22.8990, -43.1790)));
            locais.Add(new Local("Setor2", "000000F43474213000020143", new PontoMapa(-22.8990, -43.1790)));
            locais.Add(new Local("Setor3", "000000F43474213000020143", new PontoMapa(-22.8990, -43.1790)));
            locais.Add(new Local("Setor4", "000000F43474213000020143", new PontoMapa(-22.8990, -43.1790)));
            locais.Add(new Local("Setor5", "300833B2DDD906C000000000",new PontoMapa(-22.9020,-43.1815)));


            XMLSerializerList.Save<Local>(locais);
        }

        private void InserirInventario()
        {
            if (bens == null || bens.Count == 0)
                return;

            foreach (Bem item in bens)
            {
                item.ObtemInventarios().Add(new Inventario(new Local("local 1", "0010101"), new Pessoa("pessoa 1", "3253696565")));
                item.ObtemInventarios().Add(new Inventario(new Local("local 2", "00566756710101"), new Pessoa("pessoa 2", "56457")));
                item.ObtemInventarios().Add(new Inventario(new Local("local 3", "00566756710101"), new Pessoa("pessoa 3", "56457")));
                item.ObtemInventarios().Add(new Inventario(new Local("local 4", "00566756710101"), new Pessoa("pessoa 4", "56457")));
                item.ObtemInventarios().Add(new Inventario(new Local("local 5", "00566756710101"), new Pessoa("pessoa 5", "56457")));
            }
        }

        #endregion DadosBasicosDemo

        #region Debug
        public void AbreDebug()
        {
            MemoriaGrid.ItemsSource = dispositivoAtual.Memory.Tags;
            MemoriaGrid.Items.Refresh();
            DebugCanvas.Visibility = Visibility.Visible;
        }
        private void RegistraEventoLog(string texto)
        {
            RegistraEventoLogThread(texto);
        }
        private void RegistraEventoLogThread(string texto)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => EventoLog(texto));
                return;
            }
            EventoLog(texto);
        }
        private void EventoLog(string texto)
        {
            LogTxb.Text += "\n";
            LogTxb.Text += texto;
        }
        private void LoginFalso()
        {
            pessoaLogada = new Pessoa("Mauricio", "123");
            Login();
        }

        private void AtualizaMemoria()
        {
            MemoriaGrid.Items.Refresh();
        }
        #endregion Debug
    }
}
