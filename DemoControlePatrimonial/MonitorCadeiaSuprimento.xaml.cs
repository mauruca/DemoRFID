﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Ur2.RFID.Middleware;
using Ur2.RFID.Middleware.Modelo;
using Ur2.Modelo;

namespace Ur2.RFID.Demo
{
    /// <summary>
    /// Interaction logic for MonitorCadeiaSuprimento.xaml
    /// </summary>
    public partial class MonitorCadeiaSuprimento : Window
    {
        List<Local> locais = new List<Local>();
        private IDevice dispositivoAtual;

        public MonitorCadeiaSuprimento()
        {
            InitializeComponent();
        }

        public void DefineLocais(List<Local> lista)
        {
            locais = lista;
        }

        private void IniciaLeitura(bool continuo = false)
        {
            DeviceAtivo();

            if (dispositivoAtual == null)
                return;

            dispositivoAtual.Memory.Clear();
            dispositivoAtual.StartReading(continuo);
        }

        private void ParaLeitura()
        {
            if (dispositivoAtual != null)
                dispositivoAtual.StopReading();
            DeviceInativo();
        }

        private void DeviceAtivo()
        {
            AntenaAtivoBt.Visibility = Visibility.Visible;
        }

        private void DeviceInativo()
        {
            AntenaAtivoBt.Visibility = Visibility.Hidden;
        }

        private void IniciaLeituraInventario()
        {
            dispositivoAtual = DeviceFactory.CreateDevice(ConstantesDemoLocais.dispositivo);

            if (dispositivoAtual == null)
                return;

            ParaLeituraInventario();
            IniciaLeituraPeriodica();
            IniciaLeitura(true);
        }
        private void ParaLeituraInventario()
        {
            ParaLeitura();
            ParaLeituraPeriodica();
        }

        private void IniciaLeituraPeriodica()
        {
            if (dispositivoAtual == null)
                return;

            dispositivoAtual.OnTimerTick += new ChangeEventHandler(AcionaLocalCadeia);
            dispositivoAtual.StartTickerChange();
        }

        private void ParaLeituraPeriodica()
        {
            if (dispositivoAtual == null)
                return;

            dispositivoAtual.StopTickerChange();
            dispositivoAtual.OnTimerTick -= AcionaLocalCadeia;
        }

        private void AcionaLocalCadeia()
        {
            DesativaTodasAreas();

            if (dispositivoAtual == null)
                return;

            List<RFIDTag> memoryTags = dispositivoAtual.Memory.Tags;
            foreach (RFIDTag item in memoryTags)
            {
                List<Local> locais = EncontraLocais(item.Id);
                if (locais == null || locais.Count == 0)
                    continue;
                foreach (Local loc in locais)
                {
                    AtivaItemCadeia(loc);
                }
            }
            dispositivoAtual.Memory.Clear();
        }

        private List<Local> EncontraLocais(string tagid)
        {
            return locais.FindAll(item => item.Tag.Id.Contains(tagid));
        }

        private void AtivaItemCadeia(Local local)
        {
            switch (local.Nome)
            {
                case ConstantesDemoLocais.Consumidor:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => AtivaConsumidor());
                        break;
                    }
                    AtivaConsumidor();
                    break;
                case ConstantesDemoLocais.Varejo:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => AtivaVarejo());
                        break;
                    }
                    AtivaVarejo();
                    break;
                case ConstantesDemoLocais.ExpedicaoFabrica:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => AtivaExpedicaoFabrica());
                        break;
                    }
                    AtivaExpedicaoFabrica();
                    break;
                case ConstantesDemoLocais.Atacado:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => AtivaAtacado());
                        break;
                    }
                    AtivaAtacado();
                    break;
                case ConstantesDemoLocais.Fabrica:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => AtivaFabrica());
                        break;
                    }
                    AtivaFabrica();
                    break;
                default:
                    break;
            }
        }

        private void DesativaTodasAreas()
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => DesativaFabrica());
                Dispatcher.Invoke(() => DesativaAtacado());
                Dispatcher.Invoke(() => DesativaExpedicaoFabrica());
                Dispatcher.Invoke(() => DesativaVarejo());
                Dispatcher.Invoke(() => DesativaConsumidor());
                return;
            }
            DesativaFabrica();
            DesativaAtacado();
            DesativaExpedicaoFabrica();
            DesativaVarejo();
            DesativaConsumidor();
        }

        private void AtivaDesativaImagem(Image imagem, Visibility visibilidade = Visibility.Visible)
        {
            imagem.Visibility = visibilidade;
        }

        #region Areas
        private void AtivaFabrica()
        {
            AtivaDesativaImagem(FabricaAtiva);
        }
        private void DesativaFabrica()
        {
            AtivaDesativaImagem(FabricaAtiva,Visibility.Hidden);
        }
        private void AtivaAtacado()
        {
            AtivaDesativaImagem(AtacadoAtivo);
        }
        private void DesativaAtacado()
        {
            AtivaDesativaImagem(AtacadoAtivo, Visibility.Hidden);
        }
        private void AtivaExpedicaoFabrica()
        {
            AtivaDesativaImagem(ExpedicaoFabricaAtivo);
        }
        private void DesativaExpedicaoFabrica()
        {
            AtivaDesativaImagem(ExpedicaoFabricaAtivo, Visibility.Hidden);
        }
        private void AtivaVarejo()
        {
            AtivaDesativaImagem(VarejoAtivo);
        }
        private void DesativaVarejo()
        {
            AtivaDesativaImagem(VarejoAtivo, Visibility.Hidden);
        }
        private void AtivaConsumidor()
        {
            AtivaDesativaImagem(ConsumidorAtivo);
        }
        private void DesativaConsumidor()
        {
            AtivaDesativaImagem(ConsumidorAtivo, Visibility.Hidden);
        }
        #endregion Areas

        private void MoveTela(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void FecharBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ParaLeituraInventario();
            if (dispositivoAtual != null)
                dispositivoAtual.Memory.Clear();
            DesativaTodasAreas();
            this.Hide();
        }

        private void AntenaBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            IniciaLeituraInventario();
        }

        private void AntenaAtivoBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ParaLeituraInventario();
        }

        private void MoveBt_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MoveTela(sender, e);
        }
    }
}
