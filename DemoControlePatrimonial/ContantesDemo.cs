﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ur2.Modelo;
using Ur2.RFID.Middleware.Modelo;

namespace Ur2.RFID.Demo
{
    public class ConstantesDemoLocais
    {
        public static DeviceType dispositivo;

        // planta RHB
        public const string A1 = "A1";
        public const string A2 = "A2";
        public const string A3 = "A3";
        public const string A4 = "A4";
        public const string A5 = "A5";
        public const string A6 = "A6";
        public const string A7 = "A7";
        public const string A8 = "A8";
        public const string A9 = "A9";
        public const string A9P1 = "A9P1";
        public const string A9P2 = "A9P2";
        public const string A9P3 = "A9P3";
        public const string A10 = "A10";
        public const string A11 = "A11";
        public const string A12 = "A12";
        public const string A13 = "A13";
        public const string A14 = "A14";
        public const string A15 = "A15";
        // Planta Fabrica
        public const string EstoqueMateriaPrima = "Estoque Materia Prima";
        public const string Envase = "Envase";
        public const string Laboratorio = "Laboratorio";
        public const string Fabrica = "Fabrica";
        // Cadeia Suprimento
        public const string ExpedicaoFabrica = "Expedicao Fabrica";
        public const string RecebimentoAtacado = "Recebimento Atacado";
        public const string Atacado = "Atacado";
        public const string ExpedicaoAtacado = "Expedicao Atacado";
        public const string RecebimentoVarejo = "Recebimento Varejo";
        public const string Varejo = "Varejo";
        public const string ExpedicaoVarejo = "Expedicao Varejo";
        public const string Consumidor = "Consumidor";
        // Monitor EPI
        public const string Setor1 = "Setor 1";
        public const string Setor2 = "Setor 2";
        public const string Setor3 = "Setor 3";
        public const string Setor4 = "Setor 4";
        public const string Setor5 = "Setor 5";

        public static List<Local> LocaisPlantaRHB(List<Local> locais)
        {
            return locais.FindAll(loc => (loc.Nome.Equals(A1) || loc.Nome.Equals(A2) || loc.Nome.Equals(A3) || loc.Nome.Equals(A4) || loc.Nome.Equals(A5) || loc.Nome.Equals(A6) || loc.Nome.Equals(A7) || loc.Nome.Equals(A8) || loc.Nome.Equals(A9) || loc.Nome.Equals(A10) || loc.Nome.Equals(A11) || loc.Nome.Equals(A12) || loc.Nome.Equals(A13) || loc.Nome.Equals(A14) || loc.Nome.Equals(A15) || loc.Nome.Equals(A9P1) || loc.Nome.Equals(A9P2) || loc.Nome.Equals(A9P3)));
        }

        public static List<Local> LocaisObra(List<Local> locais)
        {
            return locais.FindAll(loc => (loc.Nome.Equals(Setor1) || loc.Nome.Equals(Setor2) || loc.Nome.Equals(Setor3) || loc.Nome.Equals(Setor4) || loc.Nome.Equals(Setor5)));
        }

        public static List<Local> LocaisCadeiaSuprimento(List<Local> locais)
        {
            return locais.FindAll(loc => (loc.Nome.Equals(Fabrica) || loc.Nome.Equals(ExpedicaoFabrica) || loc.Nome.Equals(RecebimentoAtacado) || loc.Nome.Equals(Atacado) || loc.Nome.Equals(ExpedicaoAtacado) || loc.Nome.Equals(RecebimentoVarejo) || loc.Nome.Equals(Varejo) || loc.Nome.Equals(ExpedicaoVarejo) || loc.Nome.Equals(Consumidor)));
        }

        public static List<Local> LocaisPlanta(List<Local> locais)
        {
            return locais.FindAll(loc => (loc.Nome.Equals(Fabrica) || loc.Nome.Equals(Envase) || loc.Nome.Equals(EstoqueMateriaPrima) || loc.Nome.Equals(Laboratorio)));
        }
    }
}
