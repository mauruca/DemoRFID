﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Ur2.RFID.Middleware;
using Ur2.RFID.Middleware.Modelo;
using Ur2.Modelo;

namespace Ur2.RFID.Demo
{
    /// <summary>
    /// Interaction logic for MonitorPlantaRHB.xaml
    /// </summary>
    public partial class MonitorPlantaRHB : Window
    {
        List<Local> locais = new List<Local>();
        private IDevice dispositivoAtual;

        public MonitorPlantaRHB()
        {
            InitializeComponent();
            DesativaTodasAreas();
        }

        public void DefineLocais(List<Local> lista)
        {
            locais = lista;
        }

        private void IniciaLeitura(bool continuo = false)
        {
            if (dispositivoAtual == null)
                return;

            dispositivoAtual.ConfigTickerChangeElapseTime();
            DeviceAtivo();
            dispositivoAtual.Memory.Clear();
            dispositivoAtual.StartReading(continuo);
        }

        private void ParaLeitura()
        {
            if (dispositivoAtual != null)
                dispositivoAtual.StopReading();
            DeviceInativo();
        }

        private void DeviceAtivo()
        {
            AntenaAtivoBt.Visibility = Visibility.Visible;
        }

        private void DeviceInativo()
        {
            AntenaAtivoBt.Visibility = Visibility.Hidden;
        }

        private void IniciaLeituraInventario()
        {
            dispositivoAtual = DeviceFactory.CreateDevice(ConstantesDemoLocais.dispositivo);

            if (dispositivoAtual == null)
                return;

            ParaLeituraInventario();
            IniciaLeituraPeriodica();
            IniciaLeitura(true);
        }
        private void ParaLeituraInventario()
        {
            ParaLeitura();
            ParaLeituraPeriodica();
        }

        private void IniciaLeituraPeriodica()
        {
            if (dispositivoAtual == null)
                return;

            dispositivoAtual.OnTimerTick += new ChangeEventHandler(AcionaLocalPlanta);
            dispositivoAtual.StartTickerChange();
        }

        private void ParaLeituraPeriodica()
        {
            if (dispositivoAtual == null)
                return;
            dispositivoAtual.StopTickerChange();
            dispositivoAtual.OnTimerTick -= new ChangeEventHandler(AcionaLocalPlanta);
        }

        private void AcionaLocalPlanta()
        {
            DesativaTodasAreas();

            if (dispositivoAtual == null)
                return;

            List<RFIDTag> memoryTags = dispositivoAtual.Memory.Tags;
            foreach (RFIDTag item in memoryTags)
            {
                List<Local> locais = EncontraLocais(item.Id);
                if (locais == null || locais.Count == 0)
                    continue;
                foreach (Local loc in locais)
                {
                    AtivaItemPlanta(loc);
                }
            }
            dispositivoAtual.Memory.Clear();
        }

        private List<Local> EncontraLocais(string tagid)
        {
            return locais.FindAll(item => item.Tag.Id.Contains(tagid));
        }

        private void AtivaItemPlanta(Local local)
        {
            switch (local.Nome)
            {
                case ConstantesDemoLocais.A1:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => VisibilidadeRetangulo(A1,Visibility.Visible));
                        break;
                    }
                    VisibilidadeRetangulo(A1, Visibility.Visible);
                    break;
                case ConstantesDemoLocais.A2:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => VisibilidadeRetangulo(A2, Visibility.Visible));
                        break;
                    }
                    VisibilidadeRetangulo(A2, Visibility.Visible);
                    break;
                case ConstantesDemoLocais.A3:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => VisibilidadeRetangulo(A3, Visibility.Visible));
                        break;
                    }
                    VisibilidadeRetangulo(A3, Visibility.Visible);
                    break;
                case ConstantesDemoLocais.A4:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => VisibilidadeRetangulo(A4, Visibility.Visible));
                        break;
                    }
                    VisibilidadeRetangulo(A4, Visibility.Visible);
                    break;
                case ConstantesDemoLocais.A5:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => VisibilidadeRetangulo(A5, Visibility.Visible));
                        break;
                    }
                    VisibilidadeRetangulo(A5, Visibility.Visible);
                    break;
                case ConstantesDemoLocais.A6:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => VisibilidadeRetangulo(A6, Visibility.Visible));
                        break;
                    }
                    VisibilidadeRetangulo(A6, Visibility.Visible);
                    break;
                case ConstantesDemoLocais.A7:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => VisibilidadeRetangulo(A7, Visibility.Visible));
                        break;
                    }
                    VisibilidadeRetangulo(A7, Visibility.Visible);
                    break;
                case ConstantesDemoLocais.A8:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => VisibilidadeRetangulo(A8, Visibility.Visible));
                        break;
                    }
                    VisibilidadeRetangulo(A8, Visibility.Visible);
                    break;
                case ConstantesDemoLocais.A9:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => VisibilidadeRetangulo(A9, Visibility.Visible));
                        break;
                    }
                    VisibilidadeRetangulo(A9, Visibility.Visible);
                    break;
                case ConstantesDemoLocais.A10:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => VisibilidadeRetangulo(A10, Visibility.Visible));
                        break;
                    }
                    VisibilidadeRetangulo(A10, Visibility.Visible);
                    break;
                case ConstantesDemoLocais.A11:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => VisibilidadeRetangulo(A11, Visibility.Visible));
                        break;
                    }
                    VisibilidadeRetangulo(A11, Visibility.Visible);
                    break;
                case ConstantesDemoLocais.A12:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => VisibilidadeRetangulo(A12, Visibility.Visible));
                        break;
                    }
                    VisibilidadeRetangulo(A12, Visibility.Visible);
                    break;
                case ConstantesDemoLocais.A13:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => VisibilidadeRetangulo(A13, Visibility.Visible));
                        break;
                    }
                    VisibilidadeRetangulo(A13, Visibility.Visible);
                    break;
                case ConstantesDemoLocais.A14:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => VisibilidadeRetangulo(A14, Visibility.Visible));
                        break;
                    }
                    VisibilidadeRetangulo(A14, Visibility.Visible);
                    break;
                case ConstantesDemoLocais.A15:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => VisibilidadeRetangulo(A15, Visibility.Visible));
                        break;
                    }
                    VisibilidadeRetangulo(A15, Visibility.Visible);
                    break;
                case ConstantesDemoLocais.A9P1:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => VisibilidadeRetangulo(A9P1, Visibility.Visible));
                        break;
                    }
                    VisibilidadeRetangulo(A9P1, Visibility.Visible);
                    break;
                case ConstantesDemoLocais.A9P2:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => VisibilidadeRetangulo(A9P2, Visibility.Visible));
                        break;
                    }
                    VisibilidadeRetangulo(A9P2, Visibility.Visible);
                    break;
                case ConstantesDemoLocais.A9P3:
                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => VisibilidadeRetangulo(A9P3, Visibility.Visible));
                        break;
                    }
                    VisibilidadeRetangulo(A9P3, Visibility.Visible);
                    break;
                default:
                    break;
            }
        }

        private void DesativaTodasAreas()
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => VisibilidadeRetangulo(A1, Visibility.Hidden));
                Dispatcher.Invoke(() => VisibilidadeRetangulo(A2, Visibility.Hidden));
                Dispatcher.Invoke(() => VisibilidadeRetangulo(A3, Visibility.Hidden));
                Dispatcher.Invoke(() => VisibilidadeRetangulo(A4, Visibility.Hidden));
                Dispatcher.Invoke(() => VisibilidadeRetangulo(A5, Visibility.Hidden));
                Dispatcher.Invoke(() => VisibilidadeRetangulo(A6, Visibility.Hidden));
                Dispatcher.Invoke(() => VisibilidadeRetangulo(A7, Visibility.Hidden));
                Dispatcher.Invoke(() => VisibilidadeRetangulo(A8, Visibility.Hidden));
                Dispatcher.Invoke(() => VisibilidadeRetangulo(A9, Visibility.Hidden));
                Dispatcher.Invoke(() => VisibilidadeRetangulo(A9P1, Visibility.Hidden));
                Dispatcher.Invoke(() => VisibilidadeRetangulo(A9P2, Visibility.Hidden));
                Dispatcher.Invoke(() => VisibilidadeRetangulo(A9P3, Visibility.Hidden));
                Dispatcher.Invoke(() => VisibilidadeRetangulo(A10, Visibility.Hidden));
                Dispatcher.Invoke(() => VisibilidadeRetangulo(A11, Visibility.Hidden));
                Dispatcher.Invoke(() => VisibilidadeRetangulo(A12, Visibility.Hidden));
                Dispatcher.Invoke(() => VisibilidadeRetangulo(A13, Visibility.Hidden));
                Dispatcher.Invoke(() => VisibilidadeRetangulo(A14, Visibility.Hidden));
                Dispatcher.Invoke(() => VisibilidadeRetangulo(A15, Visibility.Hidden));
                return;
            }
            VisibilidadeRetangulo(A1, Visibility.Hidden);
            VisibilidadeRetangulo(A2, Visibility.Hidden);
            VisibilidadeRetangulo(A3, Visibility.Hidden);
            VisibilidadeRetangulo(A4, Visibility.Hidden);
            VisibilidadeRetangulo(A5, Visibility.Hidden);
            VisibilidadeRetangulo(A6, Visibility.Hidden);
            VisibilidadeRetangulo(A7, Visibility.Hidden);
            VisibilidadeRetangulo(A8, Visibility.Hidden);
            VisibilidadeRetangulo(A9, Visibility.Hidden);
            VisibilidadeRetangulo(A9P1, Visibility.Hidden);
            VisibilidadeRetangulo(A9P2, Visibility.Hidden);
            VisibilidadeRetangulo(A9P3, Visibility.Hidden);
            VisibilidadeRetangulo(A10, Visibility.Hidden);
            VisibilidadeRetangulo(A11, Visibility.Hidden);
            VisibilidadeRetangulo(A12, Visibility.Hidden);
            VisibilidadeRetangulo(A13, Visibility.Hidden);
            VisibilidadeRetangulo(A14, Visibility.Hidden);
            VisibilidadeRetangulo(A15, Visibility.Hidden);
        }

        delegate void AtivaItem();

        #region Areas
        private void VisibilidadeRetangulo(Rectangle retangulo, Visibility visibilidade = Visibility.Hidden)
        {
            retangulo.Visibility = visibilidade;
        }
        #endregion Areas

        private void MoveTela(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void FecharBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ParaLeituraInventario();
            if (dispositivoAtual != null)
               dispositivoAtual.Memory.Clear();
            DesativaTodasAreas();
            this.Hide();
        }

        private void AntenaBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            IniciaLeituraInventario();
        }

        private void AntenaAtivoBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ParaLeituraInventario();
        }
        
        private void MoveBt_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MoveTela(sender, e);
        }
    }
}
